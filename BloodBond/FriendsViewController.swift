//
//  FriendsViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/15/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
  class Friend {
    var user_id: Int!
    var name: String!
    var fbId: String!
    var bloodGroup: String!
    var picUrl: String!
    
  }
  
  @IBOutlet var tableView: UITableView!
  var user = User()
  var allFriends = [Friend]()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Friends"
    self.navigationController?.navigationBar.translucent = false
    println("posted user fbid \(user.fbID)")
    self.showHud()
    let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "\(user.fbID)/friends", parameters: ["fields":"id,name,picture.width(480).height(480)"])
    graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
      
      if ((error) != nil)
      {
        // Process error
        println("Error: \(error)")
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          self.hideHud()
          let alert = UIAlertView(title: "Error!", message: "Unsupported operation", delegate: nil, cancelButtonTitle: "Ok")
          alert.show()
        })
      }
      else
      {
        
        //  println("json \(result)")
        if let dataAry: [AnyObject] = result["data"] as? Array{
          for object in dataAry{
            
            var friend = Friend()
            friend.fbId = object.valueForKey("id") as! String
            friend.name = object.valueForKey("name") as! String
            
            if let picData: AnyObject = object["picture"]{
              if let data: AnyObject = picData.valueForKey("data"){
                if let picUrl: String = data.valueForKey("url") as? String{
                  friend.picUrl = picUrl
                }
              }
            }
            
            self.checkFriendInAppDB(friend, completion: { (result) -> Void in
              
              if result{
                self.allFriends.append(friend)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                  self.hideHud()
                  self.tableView.reloadData()
                })
              }else{
                println("\(friend.name) doesn't exist in app db")
              }
            })
            
            
          }
          
          
          
          
        }else{
          println("no friend found")
        }
        
      }
      
      // Do any additional setup after loading the view.
    })
  }
  
  
  
  
  // MARK: tableView Datasource
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.allFriends.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: ImageCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ImageCell
    // setImageForIndexPath(indexPath)
    cell.titleLabel.text = allFriends[indexPath.row].name
    cell.subtitleLabel.text = allFriends[indexPath.row].bloodGroup
    
    if cell.customImageView?.image == nil{
      
      let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
      
      indicator.startAnimating()
      indicator.hidesWhenStopped = true
      cell.contentView.addSubview(indicator)
      cell.customImageView.layer.cornerRadius = 40
      cell.customImageView.layer.borderColor = UIColor.whiteColor().CGColor
      cell.customImageView.clipsToBounds = true
      indicator.center = cell.customImageView.center
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
        
        let aUrl = NSURL(string: self.allFriends[indexPath.row].picUrl)
        var data = NSData(contentsOfURL: aUrl!)
        let image = UIImage(data: data!)
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          cell.customImageView?.image = image
          indicator.removeFromSuperview()
        })
        
      })
    }
    
    
    return cell
  }
  
  
  
  func checkFriendInAppDB(user: Friend, completion:(result: Bool)-> Void){
    var dic = [
      "action":"getFriends",
      "fbid": user.fbId
    ]
    
    BloodBond.sharedInstance.postRequest("http://dev.bloodbond.co/api", body: dic) { (success, JSON) -> Void in
      //  println(JSON)
      if let checkFlag: AnyObject = JSON.valueForKey("getFriends"){
        
        if "\(checkFlag)" == "0"{
          
          completion(result: false)
          
        }else{
          
          if let data: [AnyObject] = JSON["data"] as? Array{
            if data.count>0 {
              let object: AnyObject? = data[0]
              let groupTag = object?.valueForKey("group") as! Int
              user.bloodGroup = self.getBloodGroup(groupTag)
              let user_id = object?.valueForKey("id") as! Int
              user.user_id = user_id
              
              completion(result: true)
            }else{
              completion(result: false)
            }
            
            
          }
          
          
        }
        
      }else{
        
        // println("check user data not found")
      }
      
      
    }
    
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if self.allFriends[indexPath.row].user_id != nil {
      
      var storyboard = UIStoryboard(name: "Main", bundle: nil)
      var posteUserProfileVC = storyboard.instantiateViewControllerWithIdentifier("PostedUserProfileVC") as! PostedUserProfileViewController
      posteUserProfileVC.postedUser = makePostedUserProfile(self.allFriends[indexPath.row])
      posteUserProfileVC.userId = String(self.allFriends[indexPath.row].user_id)
      self.navigationController?.pushViewController(posteUserProfileVC, animated: true)
      
    }else{
      let alert = UIAlertView(title: "Error!", message: "User id not Found", delegate: nil, cancelButtonTitle: "Ok")
      alert.show()
    }
  }
  
  
  func makePostedUserProfile(friend: Friend)->User{
    var user = User()
    user.fbID = friend.fbId
    user.gender = "loading..."
    user.name = friend.name
    user.hometown = "loading..."
    user.location = "loading..."
    user.email = "loading..."
    user.birthday = "loading"
    user.pictureUrl = friend.picUrl
    return user
    
  }
  
  // MARK: Get blood group string from tag
  
  func getBloodGroup(tag: Int)-> String{
    switch tag {
    case 1:return "A-"
    case 2: return "A+"
    case 3:return "B-"
    case 4: return "B+"
    case 5:return "O-"
    case 6: return "O+"
    case 7: return "AB-"
    default: return "AB+"
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
