//
//  UserDetails.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/10/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import Foundation

class UserDetails: User {
  var fbID: String!
  var name: String!
  var gender: String!
  var birthday: String!
  var work = "Unknown at unknown"
  var location: String!
  var hometown: String!
  var email: String!
  var mobile = ""
  var fbProfileLink: String!
  // var picture: UIImage!
  var pictureUrl: String!
  var cordinate: Location!
  
  override init(){
    
  }
}
