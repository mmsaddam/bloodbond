//
//  ViewExtension.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/5/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

extension UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
  func makeViewBLur(frame: CGRect) -> UIView{
    var blurView = UIView()
    blurView.frame = frame
    blurView.backgroundColor = UIColor.blackColor()
    //        let blurEffect = UIBlurEffect(style: .Dark)
    //        blurView = UIVisualEffectView(effect: blurEffect)
    //        blurView.setTranslatesAutoresizingMaskIntoConstraints(false)
    blurView.layer.opacity = 0.6
    // self.superview?.endEditing(true)
    return blurView
  }
    
    func showMe(){
        self.alpha = 0
        self.transform = CGAffineTransformMakeScale(0.1, 0.1)
        self.alpha = 1
        [UIView .animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
            self.transform = CGAffineTransformMakeScale(1.0, 1.0)
            }, completion: nil
            )]
    }
    
    func hideMe(){
        [UIView .animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
            self.transform = CGAffineTransformMakeScale(0.1, 0.1)
            }, completion: { (finised) -> Void in
                self.alpha = 0
                self.transform = CGAffineTransformMakeScale(1.0, 1.0)
        })]
    }

}
