//
//  ViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
  
  @IBOutlet var donateBloodLabel: UILabel!
  @IBOutlet var bloodBondLabel: UILabel!
  @IBOutlet var footerLabel: UILabel!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var letsStartButton: UIButton!
  var requestPermission: String!
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    
    self.letsStartButton.backgroundColor = Color.themeColor
    self.letsStartButton.layer.cornerRadius = 7
    self.footerLabel.backgroundColor = Color.themeColor
    self.donateBloodLabel.textColor = Color.themeColor
    self.bloodBondLabel.textColor = Color.themeColor
    
    if (FBSDKAccessToken.currentAccessToken() != nil){
      // User is already logged in
      self.returnFBInformation()
      
    }else{
      
      println("user logged out")
    }
  
  }
  
  override func viewDidLayoutSubviews() {
    self.addIcon()
    
  }
  
  func addIcon(){
    
    var iconView1 = IconView(frame: CGRectMake(5, 2.5, CGRectGetWidth(scrollView.frame)-10, 200), image: UIImage(named: "question.png")!, title: "Why Donate Blood", text:"Blood donation is a noble, selfless service! It gives the donor a feeling of joy and contentment. Also this is an expression of love for Mankind, as blood knows no caste, colour, creed, religion or race, country, continent or sex. Above all your blood can save a life")
    
    scrollView.addSubview(iconView1)
    
    var iconView2 = IconView(frame: CGRectMake(5, CGRectGetMaxY(iconView1.frame)+10, CGRectGetWidth(scrollView.frame)-10, 200), image: UIImage(named: "watch.png")!, title: "How often", text:"You must wait at least eight weeks (56 days) between donations of whole blood and 16 weeks (112 days) between double red cell donations. Platelet apheresis donors may give every 7 days up to 24 times per year. Regulations are different for those giving blood for themselves (autologous donors).")
    
    scrollView.addSubview(iconView2)
    
    var iconView3 = IconView(frame: CGRectMake(5, CGRectGetMaxY(iconView2.frame)+10, CGRectGetWidth(scrollView.frame)-10, 200), image: UIImage(named: "bloodbond.png")!, title: "Why Bloodbond", text:"The blood you donate gives someone another chance at life and Bloodbond allows us to serve our neighbors who rely on generous donations of this literally life-giving fluid. One day that someone may be a close relative, a friend, a family member, a loved one or even you. Help sharing this invaluable gift with the person we care for in need.")
    
    scrollView.addSubview(iconView3)
    
    scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame), CGRectGetMaxY(iconView3.frame)+30)
    
  }
  
  // MARK: Button Action

  @IBAction func supportAction(sender: AnyObject) {
    UIApplication.sharedApplication().openURL(NSURL(string: "http://bloodbond.co/support")!)
  }
  
  @IBAction func termsAction(sender: AnyObject) {
    UIApplication.sharedApplication().openURL(NSURL(string: "http://bloodbond.co/terms")!)
  }
  @IBAction func privacyAction(sender: AnyObject) {
    UIApplication.sharedApplication().openURL(NSURL(string: "http://bloodbond.co/privacypolicy")!)
  }
  
  @IBAction func startBtnAction(sender: AnyObject) {
    
    var fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
    // fbLoginManager.loginBehavior = .Web
    
    fbLoginManager.logInWithReadPermissions(["public_profile", "email", "user_friends","user_birthday","user_hometown","user_location","user_about_me"], handler: { (result, error) -> Void in
      
      if ((error) != nil){
        // Process error
        let alert = UIAlertView(title: "Error!!", message: "Failed to login", delegate: nil, cancelButtonTitle: "OK")
        alert.show()
        
      }
      else if result.isCancelled {
        // Handle cancellations
        println("cancel login...")
      }
      else {
        self.returnFBInformation()
        
      }
      
    })
    
    
  }
  
  
  // segue to homeviewcontroller
  
  func goToHome(){
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      appDelegate.createMenuView()
      
    })
    
  }
  
  // segue to profile
  func gotoSignIn(){
    
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      
      self.performSegueWithIdentifier("ModalViewController", sender: self)
    })
    
    
  }
  
  // fetch user permit data
  
  func returnFBInformation(){
    let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture.width(480).height(480),birthday,hometown,location,gender,work"])
    graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
      
      if ((error) != nil){
        // Process error
        println("Error: \(error)")
      }
      else{
        
        // fetch user data comes from facebook
        //   println(result)
        var loggedUser = BloodBond.sharedInstance.fetchUserFromFacebook(result)
        loggedUser.isLoggedIn = true
        BloodBond.sharedInstance.loggedUser = loggedUser
        
        //  BloodBond.sharedInstance.printUserDetils(loggedUser)
        
        // check user existence at database
        self.checkUserExistInDB(loggedUser, completion: { (result) -> Void in
          
          if result == true{
            // if user exist the goto home page
            self.goToHome()
            
            
          }else{
            
            /****  not exist at database then save user into database ****/
            self.saveUser(loggedUser, completion: { (result) -> Void in
              if result == true{
                var user = BloodBond.sharedInstance.loggedUser
                BloodBond.sharedInstance.printUserDetils(user)
                /**** got to signin page ****/
                self.gotoSignIn()
                
                
              }else{
                println("fail to save into database")
              }
            })
            
            
          }
        })
        
      }
    })
    
  }
  
  
  func checkUserExistInDB(user: User, completion:(result: Bool)-> Void){
    var dic = [
      "action":"checkUser",
      "fbid": user.fbID
    ]
    
    BloodBond.sharedInstance.postRequest("http://dev.bloodbond.co/api", body: dic) { (success, JSON) -> Void in
      
      // println(JSON)
      
      if success == false{
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          let alert = UIAlertView(title: "Ops!", message: "Server not responding", delegate: nil, cancelButtonTitle: "Ok")
          alert.show()
        })
        
        
      }else if let checkFlag: AnyObject = JSON.valueForKey("checkUser"){
        if "\(checkFlag)" == "0"{
          completion(result: false)
          
        }else{
          
          if let data: [AnyObject] = JSON["data"] as? Array{
            let object: AnyObject? = data[0]
            let user_id = object?.valueForKey("id") as! Int
            let group_id = object?.valueForKey("group") as! Int
            // update value from app database
            BloodBond.sharedInstance.loggedUser.mobile = object?.valueForKey("cell") as! String
            BloodBond.sharedInstance.loggedUser.email = object?.valueForKey("email") as! String
            BloodBond.sharedInstance.loggedUser.gender = object?.valueForKey("gender") as! String
            BloodBond.sharedInstance.loggedUser.work = object?.valueForKey("job") as! String
            BloodBond.sharedInstance.loggedUser.user_id = String(user_id)
            BloodBond.sharedInstance.loggedUser.birthday = object?.valueForKey("dob") as! String
            BloodBond.sharedInstance.loggedUser.bloodGroup = BloodBond.sharedInstance.getBloodGroup(group_id)
            
            completion(result: true)
          }
          
          
        }
        
      }else{
        
        
        // println("check user data not found")
      }
      
      
    }
    
  }
  
  // save user into local database
  
  func saveUser(usr: User, completion:(result: Bool)-> Void){
    var dic = [
      "fbid" : usr.fbID,
      "name" : usr.name,
      "action" : "save",
      "email" : usr.email,
      "dob" : usr.birthday,
      "gender" : usr.gender,
      "work" : usr.work,
      "location" : usr.location,
      "hometown" : usr.hometown,
    ]
    
    //  BloodBond.sharedInstance.printUserDetils(usr)
  
    BloodBond.sharedInstance.postRequest("http://dev.bloodbond.co/api", body: dic) { (success, JSON) -> Void in
      
      if success == true{
        println("save data\(JSON)")
        if let status: AnyObject? = JSON["save"]{
          if let data: [AnyObject] = JSON["data"] as? Array{
            let object: AnyObject? = data[0]
            var user_id = object!.valueForKey("id") as! Int
            BloodBond.sharedInstance.loggedUser.user_id = "\(user_id)"
            completion(result: true)
          }
        }
        
      }else{
        println("fail save request")
        completion(result: false)
      }
      
    }
    
  }
  
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
}

