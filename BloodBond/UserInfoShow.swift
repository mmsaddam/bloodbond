//
//  UserInfoShow.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/4/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class UserInfoShow: UIView {
    var textLabel: UILabel!
    let K_font_size = 13
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, iconImage: UIImage, text: String) {
        super.init(frame: frame)
        var height = CGRectGetHeight(frame)
      
        var icon = UIImageView(frame: CGRectMake((height-14)/2, 0, 14, 14))
        icon.image = iconImage
        self.addSubview(icon)
        textLabel = UILabel(frame: CGRectMake(23, (height-14)/2, CGRectGetWidth(frame)-23, 14))
        textLabel.text = text
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.font = UIFont.systemFontOfSize(CGFloat(K_font_size))
        self.addSubview(textLabel)
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
