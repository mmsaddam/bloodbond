//
//  PostModel.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/6/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class PostModel: NSObject {
    var posterImg: UIImage?
    var postOwner: String!
    var postOwnerFbId: AnyObject!
    var bloodGroup: String!  // type code for blood group
    var contact_details: String! // contact details for the post
    var created_at: String!   // post creation date
    var flag: Int!
    var hospital_address: String!  // hospital address for the posted patient
    var id: Int!
    var isDelete: Int!
    var patients_problem: String!
    var post_time: String!
    var title: String!
    var updated_at: String!
    var user_id: Int!
    var status: Int!
    var donate_date: String!
    var details: String!
    var showDetails = false
    var reportFlag = false

    
    override init() {
        super.init()
    }
    
    init(_postOwner: String, _postOwnerFbId: AnyObject, title: String, _details: String, donateDate: String, id:Int, _bloodGroup: Int, contactDetails: String, createdAt: String, flag: Int, hospitalAddress: String,isDelete: Int, patientProblem: String, postTime: String, updateAt: String, userId: Int) {
        super.init()
        self.postOwner = _postOwner
        self.postOwnerFbId = _postOwnerFbId
        self.title = title
        self.details = _details
        self.donate_date = donateDate
        self.id = id
        self.bloodGroup = getBloodGroup(_bloodGroup)
        self.contact_details = contactDetails
        self.created_at = createdAt
        self.flag = flag
        self.hospital_address = hospitalAddress
        self.isDelete = isDelete
        self.patients_problem = patientProblem
        self.post_time = postTime.toDate
        self.updated_at = updateAt
        self.user_id = userId
        
    }
    
  
  
    func getBloodGroup(tag: Int)-> String{
        switch tag {
        case 1:return "A-"
        case 2: return "A+"
        case 3:return "B-"
        case 4: return "B+"
        case 5:return "O-"
        case 6: return "O+"
        case 7: return "AB-"
        default: return "AB+"
        }
    }
}
