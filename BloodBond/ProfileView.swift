//
//  ProfileView.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/4/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class ProfileView: UIView {
  var gender: UserInfoShow!
  var work: UserInfoShow!
  var location: UserInfoShow!
  var email: UserInfoShow!
  var cell: UserInfoShow!
  private var PP: UIImageView!
  private var indicator: UIActivityIndicatorView!
  private var user: User!
  private var privacy: Privacy!
  private  var reportUser: UserInfoShow!
  
  var postReq: UIButton!
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
  
  init(frame: CGRect, user: User) {
    super.init(frame: frame)

    self.user = user
    self.privacy = user.privacy
    
    var width = CGRectGetWidth(frame)
    var height = CGRectGetHeight(frame)
    PP = UIImageView(frame: CGRectMake(10, height/2-60, 100, 100))
    PP.layer.cornerRadius = 50
    PP.layer.borderColor = UIColor.whiteColor().CGColor
    PP.layer.borderWidth = 2
    PP.clipsToBounds = true
    
    self.addSubview(PP)
    
    indicator = UIActivityIndicatorView()
    indicator.activityIndicatorViewStyle = .Gray
    indicator.center = PP.center
    indicator.startAnimating()
    self.addSubview(indicator)
    
    postReq = UIButton(frame: CGRectMake(CGRectGetMidX(PP.frame)-38, height-43, 76, 25))
    postReq.backgroundColor = Color.themeColor
    postReq.setTitle("Post Request", forState: UIControlState.Normal)
   // postReq.titleLabel?.font = UIFont.systemFontOfSize(10)
    postReq.titleLabel?.font = UIFont.boldSystemFontOfSize(10)

    postReq.layer.cornerRadius = 7
    self.addSubview(postReq)
    
    var name = UILabel(frame: CGRectMake(CGRectGetMaxX(PP.frame)+30, 4, width-(CGRectGetMaxX(PP.frame)+(20+5)), 30))
    name.font = UIFont(name: fontName, size: FontSize.PostedUserFontSize)
    name.textColor = Color.themeColor
    name.text = user.name
    self.addSubview(name)
    
    
    
    gender = UserInfoShow(frame: CGRectMake(CGRectGetMinX(name.frame), CGRectGetMaxY(name.frame), CGRectGetWidth(name.frame), 14), iconImage: UIImage(named: "gender.png")!, text: user.gender)
    self.addSubview(gender)
    
    var reuseFrame = gender.frame
    
    if privacy.job == 1{
       work = UserInfoShow(frame: CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+5, CGRectGetWidth(name.frame), 14), iconImage: UIImage(named: "job.png")!, text: user.work)
      self.addSubview(work)
      reuseFrame = work.frame
    }
    
    if privacy.location == 1{
       location = UserInfoShow(frame: CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+5, CGRectGetWidth(name.frame), 14), iconImage: UIImage(named: "location.png")!, text: user.location)
      self.addSubview(location)
      reuseFrame = location.frame
    }
    
    if privacy.email == 1{
       email = UserInfoShow(frame: CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+5, CGRectGetWidth(name.frame), 14), iconImage: UIImage(named: "message.png")!, text: user.email)
      self.addSubview(email)
      reuseFrame = email.frame
    }
    
    if privacy.cell == 1{
      var cell = UserInfoShow(frame: CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+5, CGRectGetWidth(name.frame), 14), iconImage: UIImage(named: "mobile.png")!, text: user.mobile)
      self.addSubview(cell)
       reuseFrame = cell.frame
    }
    
    
    var facebook = UserInfoShow(frame: CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+5, 130, 14), iconImage: UIImage(named: "facebook.png")!, text: "Facebook Profile")
    facebook.userInteractionEnabled = true
    reuseFrame = facebook.frame
    let tapGesture = UITapGestureRecognizer(target: self, action:Selector("handleTap:"))
    facebook.addGestureRecognizer(tapGesture)
    self.addSubview(facebook)
    
    
    if user.fbID != BloodBond.sharedInstance.loggedUser.fbID{
      if user.reportFlag == false{
        // check does user reported
        reportUser = UserInfoShow(frame: CGRectMake(CGRectGetMinX(facebook.frame), CGRectGetMaxY(facebook.frame)+5, 120, 14), iconImage: UIImage(named: "fakeuser.png")!, text: "Report User")
        reportUser.userInteractionEnabled = true
        let tapOnReport = UITapGestureRecognizer(target: self, action:Selector("handleTapOnReport:"))
        reportUser.addGestureRecognizer(tapOnReport)
        reuseFrame = reportUser.frame
        self.addSubview(reportUser)
        
      }
    
    }
    
    var shareButton = FBSDKShareButton()
    shareButton.shareContent = facebookShareContent()
    shareButton.frame = CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+5, 55, 14)
    self.addSubview(shareButton)

    NSNotificationCenter.defaultCenter().postNotificationName("ProfileImageDownloadNotifcation", object: self, userInfo: ["imageView":PP,"url":user.pictureUrl,"indicator":indicator])
    PP.addObserver(self, forKeyPath: "image", options:nil, context: nil)
    
  }
  
  
  func facebookShareContent()-> FBSDKShareLinkContent{
    
    println(self.user.pictureUrl)
    var contentUrlStr = COMMON_URL + "user/" + self.user.user_id
    var contentURL = NSURL(string: contentUrlStr)
    var imageURL =  NSURL(string: self.user.pictureUrl)
    var contentTitle = self.user.name
    var contentDescription = "My Blood Group " + self.user.bloodGroup
    var shareContent = FBSDKShareLinkContent()
  //  shareContent.imageURL = imageURL
    shareContent.contentURL = contentURL
    shareContent.contentTitle = contentTitle
    shareContent.contentDescription = contentDescription
    
    return shareContent
  }
  
  override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
    if keyPath == "image" {
      indicator.stopAnimating()
    }
  }
  
  func handleTap(tap: UITapGestureRecognizer){
    var url = "http://www.facebook.com/\(self.user.fbID)"
    println("user profile url \(url)")
    UIApplication.sharedApplication().openURL(NSURL(string:"http://www.facebook.com/\(self.user.fbID)")!)
  }
  
  func handleTapOnReport(tap: UITapGestureRecognizer){
    var alert = UIAlertView()
    alert.title = "Confirmation"
    alert.message = "Are you sure?"
    alert.delegate = self
    alert.addButtonWithTitle("No")
    alert.addButtonWithTitle("Ok")
    alert.show()
    
  }
  
  func alertView(alertView: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
    if buttonIndex == 1{
      
      var reportDic = [
        "action" : "report",
        "type" : "user",
        "report_to": self.user.user_id,
        "report_by" : BloodBond.sharedInstance.loggedUser.user_id
      ]
      
      MBProgressHUD.showHUDAddedTo(self, animated: true)
      BloodBond.sharedInstance.postRequest(API_URL, body: reportDic, completion: { (success, JSON) -> Void in
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          MBProgressHUD.hideHUDForView(self, animated: true)
          
          if let status = JSON["report"] as? Int{
            if status == 1{
              self.reportUser.removeFromSuperview()
            }else{
              let alert = UIAlertView(title: "Ops!", message: "Fail to report", delegate: nil, cancelButtonTitle: "Ok")
              alert.show()
            }
            
          }
        })
        
      })
      
      
    }
    
  }
  
  // generate report key 
  // reported by user fbid + report to user fbid
  func reportKey()-> String{
    return BloodBond.sharedInstance.loggedUser.fbID + user.fbID
  }
    
  deinit{
    PP.removeObserver(self, forKeyPath: "image")
  }
  /*
  // Only override drawRect: if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func drawRect(rect: CGRect) {
  // Drawing code
  }
  */
  
}
