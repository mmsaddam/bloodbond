//
//  DonaitonCycle.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/5/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class DonaitonCycle: UIView {
  
  var circle : CircleProgressView!
  var donateBtn: UIButton!
  var lastDonate: UILabel!
  var canDonate: UILabel!
  var bloodDonate: UILabel!
  var bloodGroup: UILabel!
  var daysLabel: UILabel!
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init(frame: CGRect, user: User) {
    super.init(frame: frame)
//    self.backgroundColor = UIColor.clearColor()
//    self.layer.borderWidth = 2
//    self.clipsToBounds = true
//    self.layer.borderColor = borderColor.CGColor
//    self.layer.cornerRadius = 2
    
    var width = CGRectGetWidth(frame)
    var height = CGRectGetHeight(frame)
    
    
    let backCircle = CircleProgressView()
    backCircle.frame =  CGRectMake(15, 25, 90, 90)
    backCircle.progressLabel.alpha = 0
    backCircle.timeLimit = 120
    backCircle.elapsedTime = 120
    backCircle.tintColor = UIColor.lightGrayColor()
    self.addSubview(backCircle)

    
    circle  = CircleProgressView()
    circle.frame =  CGRectMake(15, 25, 90, 90)
    circle.center = backCircle.center
    circle.tintColor = Color.themeColor
    
    daysLabel = UILabel(frame: circle.bounds)
    daysLabel.center = circle.center
    daysLabel.backgroundColor = UIColor.clearColor()
    daysLabel.textColor = Color.themeColor
    daysLabel.textAlignment = NSTextAlignment.Center
    daysLabel.adjustsFontSizeToFitWidth = true
    self.addSubview(daysLabel)
    daysLabel.text = String(user.donation.daysSinceDonate!)
    
    circle.progressLabel.text = String(user.donation.daysSinceDonate!)
    circle.progressLabel.alpha = 0
    self.addSubview(circle)
    
   
      donateBtn = UIButton(frame: CGRectMake(CGRectGetMidX(circle.frame)-38, height-45, 76, 25))
      donateBtn.backgroundColor = Color.themeColor
      donateBtn.setTitle("I Donate", forState: UIControlState.Normal)
      donateBtn.layer.cornerRadius = 7
      donateBtn.titleLabel?.font =  UIFont.boldSystemFontOfSize(10)
    
      self.addSubview(donateBtn)
     if user.isLoggedIn == false{
      donateBtn.alpha = 0
    }
   
    
    bloodGroup = UILabel(frame: CGRectMake(CGRectGetMaxX(circle.frame)+30, 25, 60, 50))
    bloodGroup.layer.cornerRadius = 10
    bloodGroup.layer.borderColor = Color.themeColor.CGColor
    bloodGroup.layer.borderWidth = 2.0
    bloodGroup.textAlignment = NSTextAlignment.Center
    bloodGroup.textColor = Color.themeColor
    bloodGroup.font = UIFont.systemFontOfSize(30)
    bloodGroup.text = user.bloodGroup
    self.addSubview(bloodGroup)
    
    canDonate = UILabel(frame: CGRectMake(CGRectGetMinX(bloodGroup.frame), CGRectGetMaxY(bloodGroup.frame),width-CGRectGetMinX(bloodGroup.frame) , 20))
    canDonate.textColor = Color.textColor
    canDonate.numberOfLines = 0
    canDonate.adjustsFontSizeToFitWidth = true
    canDonate.font = UIFont.systemFontOfSize(15)
    canDonate.text = "Can Donate: " + "\(user.donation.nextDonateDate!)"
    self.addSubview(canDonate)
    
    lastDonate = UILabel(frame: CGRectMake(CGRectGetMinX(bloodGroup.frame), CGRectGetMaxY(canDonate.frame),width-CGRectGetMinX(bloodGroup.frame) , 20))
    lastDonate.textColor = Color.textColor
    lastDonate.numberOfLines = 0
    lastDonate.adjustsFontSizeToFitWidth = true
    lastDonate.font = UIFont.systemFontOfSize(15)
    lastDonate.text = "Last Donate: " + "\(user.donation.lastDonateDate!)"
    self.addSubview(lastDonate)
    
    bloodDonate = UILabel(frame: CGRectMake(CGRectGetMinX(bloodGroup.frame), CGRectGetMaxY(lastDonate.frame),width-CGRectGetMinX(bloodGroup.frame) , 20))
    
    bloodDonate.numberOfLines = 0
    bloodDonate.font = UIFont.systemFontOfSize(15)
    bloodDonate.adjustsFontSizeToFitWidth = true
    bloodDonate.textColor = Color.textColor
    bloodDonate.text = "Total Donate: " + String(user.donation.totalDonate!)
    self.addSubview(bloodDonate)
    
  }
  
  func userBloodGroupSet(bdGroup: String){
    bloodGroup.text = bdGroup
  }
  
  func lastBloodDoante(date: NSDate){
    let formatter = NSDateFormatter()
    formatter.dateStyle = NSDateFormatterStyle.MediumStyle
    let dateString = formatter.stringFromDate(date) as String
    lastDonate.text = "Last Donate: \(dateString)"
  }
  
  func canDonateFrom(date: NSDate){
    let formatter = NSDateFormatter()
    formatter.dateStyle = NSDateFormatterStyle.MediumStyle
    let dateString = formatter.stringFromDate(date) as String
    canDonate.text = "Can Donate: \(dateString)"
  }
  
  func dateFromString(dateStr: String)-> String{
    let formatter = NSDateFormatter()
   // formatter.dateStyle = NSDateFormatterStyle.MediumStyle
    formatter.dateFormat = "MM-yyyy"
    let date = formatter.dateFromString(dateStr)
      return "\(date)"
  }
  
  
  
  func numberOfTimeDoate(times: Int){
    bloodDonate.text = "Blood Donate: \(times) times"
  }
  
  /*
  // Only override drawRect: if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func drawRect(rect: CGRect) {
  // Drawing code
  }
  */
  
}
