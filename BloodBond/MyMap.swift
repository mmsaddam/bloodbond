//
//  MyMap.swift
//  GoogleMap
//
//  Created by Muzahidul Islam on 10/25/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit
import MapKit

class MyMap: MKMapView {
  var search: UITextField!
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  override init(frame: CGRect) {
    super.init(frame: frame)
    var toolbar = UIToolbar(frame: CGRectMake(10, 5, CGRectGetWidth(frame)-20, 44))
    search = UITextField(frame: toolbar.bounds)
    search.autocapitalizationType = .None
    search.autocorrectionType = .No
    search.returnKeyType = .Search
   // self.addSubview(search)
   
    var items = [AnyObject]()
    
    let baritem = UIBarButtonItem(customView: search)
    
   
    toolbar.setItems([baritem], animated: true)
   self.addSubview(toolbar)
   
  }
  
  
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
