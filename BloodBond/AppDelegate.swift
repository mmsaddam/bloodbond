//
//  AppDelegate.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var slideMenuController:SlideMenuController?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        UINavigationBar.appearance().barTintColor = Color.themeColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
      
      var postTime = "2015-11-03 04:47:12"
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
      var date = dateFormatter.dateFromString(postTime)
      dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
      
      let str = dateFormatter.stringFromDate(date!)
      println("date \(str)")
      
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    func createMenuView() {
        
        UINavigationBar.appearance().barTintColor = Color.themeColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        // create viewController code...
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftViewController") as! LeftViewController
        let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenuController") as! RIghtViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        leftViewController.mainViewController = nvc
        
        self.slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        self.slideMenuController?.closeLeft()
        
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    
    func logout(){
        
      
      
//      [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions/publish_actions"
//      parameters:nil
//      HTTPMethod:@"DELETE"]
//      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
//        // ...
//      }];
      
      
      var fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
      fbLoginManager.loginBehavior = .Web
      fbLoginManager.logOut()
      FBSDKAccessToken.setCurrentAccessToken(nil)
      FBSDKProfile.setCurrentProfile(nil)
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      
      var storyboard = UIStoryboard(name: "Main", bundle: nil)
      let viewController = storyboard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
      var nvc: UINavigationController = UINavigationController(rootViewController: viewController)
      nvc.navigationBar.translucent = false
      appDelegate.window?.rootViewController = nvc
      //  self.slideMenuController?.closeLeft()
      appDelegate.window?.makeKeyAndVisible()
      
//      let fbRequest = FBSDKGraphRequest(graphPath: "me/permissions/publish_actions", parameters: nil, HTTPMethod: "DELETE")
//      fbRequest.startWithCompletionHandler { (connection, result, error) -> Void in
//        
//      }
      
      
        
    }

    
    func application(application: UIApplication,
        openURL url: NSURL,
        sourceApplication: String?,
        annotation: AnyObject?) -> Bool {
            return FBSDKApplicationDelegate.sharedInstance().application(
                application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

