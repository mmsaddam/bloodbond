//
//  UpdateViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/20/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit
import MapKit

class UpdateViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
  
  let CONST_SUCCESS_TAG = 555555
  let CONST_FAIL_TAG = 444444
  enum ActiveAction: Int{
    case BDGroup = 0
    case Country
    case Birthday
  }
  @IBOutlet var emailLabel: UILabel!
  
  @IBOutlet var jobLabel: UILabel!
  @IBOutlet var phoneNoLabel: UILabel!
  @IBOutlet var birthDateLabel: UILabel!
  @IBOutlet var bloodGrpLabel: UILabel!
  @IBOutlet var scrollView: UIScrollView!
  //  @IBOutlet var homeLocField: UITextField!
  //  @IBOutlet var homeLocToolbar: UIToolbar!
  //  @IBOutlet var curLocField: UITextField!
  //  @IBOutlet var curentLocationToolbar: UIToolbar!
  @IBOutlet var birthdayToolbar: UIToolbar!
  @IBOutlet var phoneToolbar: UIToolbar!
  @IBOutlet var firstToolbar: UIToolbar!
  @IBOutlet var emailField: UITextField!
  
  @IBOutlet var emailToolbar: UIToolbar!
  @IBOutlet var jobToolbar: UIToolbar!
  @IBOutlet var jobField: UITextField!
  
  @IBOutlet var bGField: UITextField!
  @IBOutlet var phoneField: UITextField!
  @IBOutlet var countryNameField: UITextField!
  @IBOutlet var countryCodeField: UITextField!
  @IBOutlet var birthdateField: UITextField!
  
  var active = ActiveAction(rawValue: 0)
  var itemsBdGroup = ["A-", "A+", "B-", "B+", "AB+", "AB-", "O+", "O-"]
  var itemsCountry = [CountryCode]()
  var tableView: UITableView!
  var blurView: UIView!
  var datePicker: PickerView!
  var loggedUser : User!
  var curLocMap: MyMap!
  var homeLocMap: MyMap!
  
  var curLocAnnotation: ArtWork!
  var homeLocAnnotation: ArtWork!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    
    loggedUser = BloodBond.sharedInstance.loggedUser
    
    itemsCountry = [
      CountryCode(nameCode: "AF", fullName: "Afghanistan", phoneCode: "+93"),
      CountryCode(nameCode: "AL", fullName: "Albania", phoneCode: "+355"),
      CountryCode(nameCode: "DZ", fullName: "Algeria", phoneCode: "+213"),
      CountryCode(nameCode: "AS", fullName: "American Samoa", phoneCode: "+1"),
      CountryCode(nameCode: "AD", fullName: "Andorra", phoneCode: "+376"),
      CountryCode(nameCode: "AO", fullName: "Angola", phoneCode: "+244"),
      CountryCode(nameCode: "AI", fullName: "Anguilla", phoneCode: "+1"),
      CountryCode(nameCode: "AR", fullName: "Antigua", phoneCode: "+1"),
      CountryCode(nameCode: "AR", fullName: "Argentina", phoneCode: "+54"),
      CountryCode(nameCode: "AM", fullName: "Armenia", phoneCode: "+374"),
      CountryCode(nameCode: "AW", fullName: "Aruba", phoneCode: "+297"),
      CountryCode(nameCode: "AU", fullName: "Australia", phoneCode: "+61"),
      CountryCode(nameCode: "AT", fullName: "Austria", phoneCode: "+43"),
      CountryCode(nameCode: "AZ", fullName: "Azerbaijan", phoneCode: "+994")
    ]
    
    
    self.popoverTableViewSetting()
    self.scrollView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))
    self.firstToolbar.frame = CGRectMake(0, CGRectGetMinY(self.firstToolbar.frame), CGRectGetWidth(self.scrollView.frame), 44)
    self.emailToolbar.frame = CGRectMake(0, CGRectGetMinY(self.emailToolbar.frame), CGRectGetWidth(self.scrollView.frame), 44)
    self.birthdayToolbar.frame = CGRectMake(0, CGRectGetMinY(self.birthdayToolbar.frame), CGRectGetWidth(self.scrollView.frame), 44)
    self.phoneToolbar.frame = CGRectMake(0, CGRectGetMinY(self.phoneToolbar.frame), CGRectGetWidth(self.scrollView.frame), 44)
    self.jobToolbar.frame = CGRectMake(0, CGRectGetMinY(self.jobToolbar.frame), CGRectGetWidth(self.scrollView.frame), 44)
    
    bGField.addArrow()
    birthdateField.addArrow()
    
    jobLabel.attributedText = "Job"["*"]
    emailLabel.attributedText = "Email"["*"]
    birthDateLabel.attributedText = "Birth Date"["*"]
    bloodGrpLabel.attributedText = "Blood Group"["*"]
    phoneNoLabel.attributedText = "Mobile"["*"]
    
    let curloclabel = UILabel(frame: CGRectMake(16, CGRectGetMaxY(jobToolbar.frame)+5, 250, 20))
    curloclabel.attributedText = "Current Location"["*"]
    
    scrollView.addSubview(curloclabel)
    
    curLocAnnotation = ArtWork(title: loggedUser.name,
      locationName: loggedUser.location,
      discipline: loggedUser.location,
      coordinate: CLLocationCoordinate2D(latitude: 23.70, longitude: 90.37))
    curLocMap = MyMap(frame: CGRectMake(0, CGRectGetMaxY(curloclabel.frame), CGRectGetWidth(self.scrollView.frame), 220))
    curLocMap.search.placeholder = "Enter Current Location"
    curLocMap.addAnnotation(curLocAnnotation)
    curLocMap.centerCoordinate = curLocAnnotation.coordinate
    curLocMap.search.delegate = self
    self.scrollView.addSubview(curLocMap)
    
    
    
    homeLocAnnotation = ArtWork(title: loggedUser.name,
      locationName: loggedUser.hometown,
      discipline: loggedUser.hometown,
      coordinate: CLLocationCoordinate2D(latitude: 23.70, longitude: 90.37))
    
    
    let homeloclabel = UILabel(frame: CGRectMake(16, CGRectGetMaxY(curLocMap.frame)+5, 250, 20))
    homeloclabel.attributedText = "Home Location"["*"]
    scrollView.addSubview(homeloclabel)
    
    homeLocMap = MyMap(frame: CGRectMake(0, CGRectGetMaxY(homeloclabel.frame), CGRectGetWidth(self.scrollView.frame), 220))
    homeLocMap.search.placeholder = "Enter Home Location"
    homeLocMap.search.delegate = self
    homeLocMap.addAnnotation(homeLocAnnotation)
    homeLocMap.centerCoordinate = homeLocAnnotation.coordinate
    self.scrollView.addSubview(homeLocMap)
    
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(view.frame),CGRectGetMaxY(homeLocMap.frame)+250)
    
    var tapOnScroll = UITapGestureRecognizer(target: self, action: "handleTapOnScrollView:")
    scrollView.addGestureRecognizer(tapOnScroll)
    
    
    datePicker = PickerView(kframe: CGRectMake(15, CGRectGetHeight(self.view.frame)/2-100, CGRectGetWidth(self.view.frame)-30, 200))
    
    datePicker.done.addTarget(self, action: "datePickerDone:", forControlEvents: UIControlEvents.TouchUpInside)
    datePicker.alpha = 0
    
    self.view.addSubview(datePicker)
    self.setInitalFieldValueFromLoggedUser()
    
    
    
    let cancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancel:")
    self.navigationItem.leftBarButtonItem = cancel
    
    let update = UIBarButtonItem(title: "Update", style: .Plain, target: self, action: "update:")
    self.navigationItem.rightBarButtonItem = update
    
  }
  
  
  // MARK: nav iteam action
  
  @IBAction func cancel(sender: AnyObject) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  @IBAction func update(sender: AnyObject) {
    if fieldValidation(){
      self.userUpdateReq(loggedUser, completion: { (result) -> Void in
        if result == true{
          
          /**** Notify pofile that privacy changed ****/
          NSNotificationCenter.defaultCenter().postNotificationName(K_UPDATE_GENERAL_SETTINGS, object: self, userInfo: nil)
          
          var alert = UIAlertView(title: "Congratulations!", message: "Successfully Updated", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_SUCCESS_TAG
          alert.show()
        }else{
          var alert = UIAlertView(title: "Error!", message: "Fail to update", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_FAIL_TAG
          alert.show()
        }
      })
    }
    
  }
  
  // MARK: UIAlertView Delegate
  
  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
    if alertView.tag == CONST_SUCCESS_TAG{
      self.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  // MARK: setinital field value
  
  func setInitalFieldValueFromLoggedUser(){
    loggedUser = BloodBond.sharedInstance.loggedUser
    self.emailField.text = loggedUser.email.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    self.birthdateField.text = loggedUser.birthday
    self.curLocMap.search.text = loggedUser.location
    self.homeLocMap.search.text = loggedUser.hometown
    self.jobField.text = loggedUser.work
    self.phoneField.text = self.loggedUser.mobile
    self.bGField.text = self.loggedUser.bloodGroup
  }
  
  // MARK: handle tap on scrollview
  
  func handleTapOnScrollView(gesture: UITapGestureRecognizer){
    
    let location = gesture.locationInView(gesture.view)
    
    for index in 0..<scrollView.subviews.count {
      let view = scrollView.subviews[index] as! UIView
      
      if CGRectContainsPoint(view.frame, location){
        if view == firstToolbar{
          let loc = self.firstToolbar.convertPoint(location, fromView: gesture.view)
          if CGRectContainsPoint(bGField.frame, loc){
            self.bGField.resignFirstResponder()
            active = ActiveAction(rawValue: 0)
            self.tableView.reloadData()
            self.tableView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-100,CGRectGetMaxY(firstToolbar.frame), 200, 200)
            tableViewHideAndShow(true)
          }
          
        }else if view == phoneToolbar{
          
          let loc = self.phoneToolbar.convertPoint(location, fromView: gesture.view)
          if CGRectContainsPoint(countryNameField.frame, loc){
            self.phoneField.resignFirstResponder()
            active = ActiveAction(rawValue: 1)
            self.tableView.reloadData()
            self.tableView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-100,CGRectGetMaxY(phoneToolbar.frame), 200, 200)
            tableViewHideAndShow(true)
          }
          
        } else if view == birthdayToolbar{
          let loc = self.birthdayToolbar.convertPoint(location, fromView: gesture.view)
          if CGRectContainsPoint(birthdateField.frame, loc){
            self.view.endEditing(true)
            datePicker.showMe()
          }
        }
      }
      
      
    }
    
    
  }
  @IBAction func doneAction(sender: AnyObject) {
    
    if fieldValidation(){
      self.userUpdateReq(loggedUser, completion: { (result) -> Void in
        if result == true{
          var alert = UIAlertView(title: "Congratulations!", message: "Successfully Updated", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_SUCCESS_TAG
          alert.show()
        }else{
          var alert = UIAlertView(title: "Error!", message: "Fail to update", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_FAIL_TAG
          alert.show()
        }
      })
    }
  }
  
  
  func goToHome(){
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      appDelegate.createMenuView()
      
    })
    
  }
  
  // date picker done button action
  func datePickerDone(sender: UIButton){
    var formatter = NSDateFormatter()
    formatter.dateFormat = "MM/dd/yyyy"
    // formatter.dateStyle = NSDateFormatterStyle.ShortStyle
    
    birthdateField.text = formatter.stringFromDate(datePicker.date())
    datePicker.hideMe()
  }
  
  // MARK: update user
  func userUpdateReq(usr: User, completion:(result: Bool)-> Void){
    
    var dic = [
      "action" : "updateGeneralSettings",
      "id" : loggedUser.user_id,
      "email" : self.emailField.text,
      "dob" : self.birthdateField.text,
      "job" : self.jobField.text,
      "group" : String(getBloodGroup(bGField.text)),
      "cell" : self.phoneField.text,
      "location" : curLocMap.search.text,
      "cur_lat" : "\(curLocAnnotation.coordinate.latitude)",
      "cur_long" : "\(curLocAnnotation.coordinate.longitude)",
      "location2" : homeLocMap.search.text,
      "home_lat" : "\(homeLocAnnotation.coordinate.latitude)",
      "home_long" : "\(homeLocAnnotation.coordinate.longitude)"
    ]
    //  BloodBond.sharedInstance.printUserDetils(usr)
    
    println("dic\(dic)")
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      println("update: \(JSON)")
      if let updateUser: Int? = JSON.valueForKey("updateGeneralSettings") as? Int{
        if updateUser == 1{
          
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            //   BloodBond.sharedInstance.loggedUser.email = self.emailField.text
            BloodBond.sharedInstance.loggedUser.birthday = self.birthdateField.text
            BloodBond.sharedInstance.loggedUser.bloodGroup = self.bGField.text
            BloodBond.sharedInstance.loggedUser.mobile = self.phoneField.text
            BloodBond.sharedInstance.loggedUser.location = self.curLocMap.search.text
            BloodBond.sharedInstance.loggedUser.hometown = self.homeLocMap.search.text
            BloodBond.sharedInstance.loggedUser.work = self.jobField.text
            self.hideHud()
            completion(result: true)
          })
          
        }else{
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.hideHud()
            println("fail to update user")
            var alert = UIAlertView(title: "Error!!", message: "You are not matured enought", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            completion(result: false)
          })
          
        }
        
      }
      
    }
    
  }
  
  
  func getBloodGroup(tag: String)-> Int{
    switch tag {
    case "A-":return 1
    case "A+": return 2
    case "B-":return 3
    case "B+": return 4
    case "O-":return 5
    case "O+": return 6
    case "AB-": return 7
    default: return 8
    }
  }
  
  // MARK: field validation
  func fieldValidation()-> Bool{
    let alert = UIAlertView()
    alert.addButtonWithTitle("Ok")
    alert.title = "Error!!"
    
    if bGField.text.isEmpty{
      alert.message = "Please enter your blood group"
      alert.show()
      return false
    }else if emailField.text.isEmpty{
      alert.message = "Please enter your email"
      alert.show()
      return false
    }else if !emailValidation(emailField.text){
      alert.message = "Please enter valid email"
      alert.show()
      return false
    }else if birthdateField.text.isEmpty{
      alert.message = "Please enter birthday"
      alert.show()
      return false
    }else if phoneField.text.isEmpty{
      alert.message = "Please enter your phone"
      alert.show()
      return false
    }else if jobField.text.isEmpty{
      alert.message = "Please enter your job"
      alert.show()
      return false
    }else if curLocMap.search.text.isEmpty{
      alert.message = "Please enter current location"
      alert.show()
      return false
    }else if homeLocMap.search.text.isEmpty{
      alert.message = "Please enter home location"
      alert.show()
      return false
    }else{
      return true
    }
  }
  
  
  func emailValidation(email: String)-> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(email)
  }
  
  // MARK:- UITableView Datasource
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    if active?.rawValue == 0{
      return itemsBdGroup.count
    }else{
      return itemsCountry.count
    }
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    
    var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
    if active?.rawValue == 0{
      cell.textLabel?.text = itemsBdGroup[indexPath.row]
    }else{
      var country = itemsCountry[indexPath.row]
      cell.textLabel?.text = country.fullName
    }
    
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if active?.rawValue == 0{
      self.bGField.text = itemsBdGroup[indexPath.row]
      bGField.resignFirstResponder()
      tableViewHideAndShow(false)
    }else{
      var country = itemsCountry[indexPath.row]
      countryNameField.text = country.nameCode
      countryCodeField.text = country.phoneCode
      tableViewHideAndShow(false)
    }
    
  }
  
  // MARK: tableView control
  
  
  func popoverTableViewSetting(){
    tableView = UITableView(frame: CGRectMake(0, 10, 200, 200), style: UITableViewStyle.Plain)
    
    self.view.addSubview(tableView)
    tableView.dataSource = self
    tableView.delegate = self
    tableView.alpha = 0;
    self.view.bringSubviewToFront(self.tableView)
    
    
    tableView.layer.borderWidth = 1
    tableView.layer.borderColor = UIColor.grayColor().CGColor
    tableView.layer.masksToBounds = true
  }
  
  func tableViewHideAndShow(isVisible:Bool){
    if isVisible {
      self.view.bringSubviewToFront(tableView)
      self.view.endEditing(true)
    }
    removeBlur()
    isVisible ?applyBlurEffect():removeBlur()
    [UIView .animateWithDuration(0.3, animations: { () -> Void in
      self.tableView.alpha = isVisible ? 1:0
      
      }, completion:nil
      )];
    
  }
  
  // MARK: blur effect
  
  func applyBlurEffect(){
    blurView = UIView().makeViewBLur(self.view.bounds)
    view.insertSubview(blurView, belowSubview: tableView)
    
  }
  
  func removeBlur(){
    if (blurView != nil) {
      blurView.removeFromSuperview()
      blurView = nil
    }
  }
  
  // MARK: UITextField Delegate
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
    if textField == self.bGField{
      
    }
    return true
  }
  
  func textFieldDidBeginEditing(textField: UITextField){
    
  }
  
  func textFieldShouldEndEditing(textField: UITextField) -> Bool{
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool{
    textField.resignFirstResponder()
    if textField == curLocMap.search{
      self.currLocationSearch(curLocMap.search.text)
    }else if textField == homeLocMap.search{
      self.homeLocationSearch(curLocMap.search.text)
    }
    return true
  }
  
  
  func homeLocationSearch(text: String){
    if self.homeLocMap.annotations.count != 0{
      var  annotation = self.homeLocMap.annotations.first as! ArtWork
      self.homeLocMap.removeAnnotation(annotation)
    }
    var  localSearchRequest = MKLocalSearchRequest()
    localSearchRequest.naturalLanguageQuery = text
    var localSearch = MKLocalSearch(request: localSearchRequest)
    localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
      
      if localSearchResponse == nil{
        let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        return
      }
      
      let coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
      self.homeLocAnnotation = ArtWork(title: self.loggedUser.name, locationName: text, discipline: text, coordinate: coordinate)
      self.homeLocMap.addAnnotation(self.homeLocAnnotation)
      self.homeLocMap.centerCoordinate = coordinate
      
    }
    
  }
  
  func currLocationSearch(text: String){
    if self.curLocMap.annotations.count != 0{
      var  annotation = self.curLocMap.annotations.first as! ArtWork
      self.curLocMap.removeAnnotation(annotation)
    }
    let  searchText = text.stringByReplacingOccurrencesOfString(",", withString: " ", options: NSStringCompareOptions.LiteralSearch, range: nil)
    
    var  localSearchRequest = MKLocalSearchRequest()
    localSearchRequest.naturalLanguageQuery = searchText
    var localSearch = MKLocalSearch(request: localSearchRequest)
    localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
      
      if localSearchResponse == nil{
        let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        return
      }
      
      let coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
      self.curLocAnnotation = ArtWork(title: self.loggedUser.name, locationName: text, discipline: text, coordinate: coordinate)
      self.curLocMap.addAnnotation(self.curLocAnnotation)
      self.curLocMap.centerCoordinate = coordinate
      
    }
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
