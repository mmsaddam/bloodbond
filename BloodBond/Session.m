//
//  Session.m
//  CircularProgressControl
//
//  Created by Carlos Eduardo Arantes Ferreira on 22/11/14.
//  Copyright (c) 2014 Mobistart. All rights reserved.
//

#import "Session.h"

@implementation Session

- (NSTimeInterval)progressTime {
    
    if (_finishDate) {
        return [_finishDate timeIntervalSinceDate:self.startDate];
    }
    else {
        return [[NSDate date] timeIntervalSinceDate:self.startDate];
    }
}

-(NSUInteger)dayInterval{
    
    if (_finishDate) {
        
        NSTimeInterval timeInterval = [_finishDate timeIntervalSinceDate:self.startDate];
        NSUInteger interval = timeInterval/86400;
        return interval ;
    }
    else {
        
        NSTimeInterval timeInterval = [_finishDate timeIntervalSinceDate:self.startDate];
        NSUInteger interval = timeInterval/86400;
        return   interval;
    }
}
@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net 
