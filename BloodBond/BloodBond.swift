//
//  BloodBond.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/3/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class BloodBond: NSObject {
  
  var loggedUser: User!
  var donationSession: Session!
  
  private var httpClient: HTTPClient!
  private var persistencyManager: PersistenceManger
  private var parseJson: BBJson!
  
  class var sharedInstance: BloodBond{
    struct Static {
      static var instance: BloodBond!
      static var token: dispatch_once_t = 0
    }
    
    dispatch_once(&Static.token){
      Static.instance = BloodBond()
    }
    return Static.instance
  }
  
  override init() {
    
    loggedUser = User()
    httpClient = HTTPClient()
    parseJson = BBJson()
    persistencyManager = PersistenceManger()
    donationSession = Session()
    donationSession.startDate = NSDate()
    donationSession.finishDate = NSDate()
    
    super.init()
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"downloadImage:", name: "ProfileImageDownloadNotifcation", object: nil)
   
   
    
  }
  
  func loggedUserProfilePic() -> UIImage{
    return httpClient.downloadImage(loggedUser.pictureUrl)
  }
  
   
  func downloadImage(notification: NSNotification) {
    let userInfo = notification.userInfo as! [String: AnyObject]
    var imageView = userInfo["imageView"] as! UIImageView?
    var indicatror = userInfo["indicator"] as! UIActivityIndicatorView?
    let coverUrl = userInfo["url"] as! String
    
    if let imageViewUnWrapped = imageView {
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
        let downloadedImage = self.httpClient.downloadImage(coverUrl as String)
        
        dispatch_sync(dispatch_get_main_queue(), { () -> Void in
          imageViewUnWrapped.image = downloadedImage
          indicatror?.stopAnimating()
          // self.persistencyManager.saveImage(downloadedImage, filename: coverUrl.lastPathComponent)
        })
      })
    }
  }
  
  
  func postRequest(url: String, body: AnyObject, completion:(success:Bool, JSON:AnyObject)->Void){
    httpClient.postRequest(url, body: body) { (_success , _JSON) -> Void in
      completion(success: _success, JSON: _JSON)
    }
  }
  
  
  // save all posts
  
  func saveAllPosts(allPosts: [PostModel]){
    persistencyManager.saveAllPosts(allPosts)
  }
  
  // get all posts
  func getAllPosts()-> [PostModel]{
    return persistencyManager.getAllPosts()
  }
  
  // get picture url from fbid
  func getPicUrlFromFbId(fbId: AnyObject)-> String{
    var commonStr = "http://graph.facebook.com/"
    commonStr  += "\(fbId)".removeOptional()
    commonStr += "/picture?type=large"
    
    return commonStr
    
  }
  
  func fetchUserFromFacebook(json: AnyObject)-> User{
    return parseJson.parseFacebookData(json)
  }
  
  // MARK: fetch user data

  func fetchUser(json: AnyObject)-> User{
    return parseJson.fetchUserDetails(json)
  }

  
   //print user details for testing perpose
  
  func printUserDetils(user:User){
    println("isLoggedIn::\(user.isLoggedIn)")
    println("user_id::\(user.user_id)")
    println("fbID::\(user.fbID)")
    println("name::\(user.name)")
    println("gender::\(user.gender)")
    println("birthday::\(user.birthday)")
    println("work::\(user.work)")
    println("location::\(user.location)")
    println("hometown::\(user.hometown)")
    println("email::\(user.email)")
    println("mobile::\(user.mobile)")
    println("fbProfileLink::\(user.fbProfileLink)")
    println("pictureUrl::\(user.pictureUrl)")
    println("bloodGroup::\(user.bloodGroup)")
    
  }
  
   // get blood gruoup from tag
  func getBloodGroup(tag: Int)-> String{
    switch tag {
    case 1:return "A-"
    case 2: return "A+"
    case 3:return "B-"
    case 4: return "B+"
    case 5:return "O-"
    case 6: return "O+"
    case 7: return "AB-"
    default: return "AB+"
    }
  }
  
 
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
}


