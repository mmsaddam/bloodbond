//
//  SettingsViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

  @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.navigationBar.translucent = false
      
        self.title = "Settings"
        self.tableView.frame = self.view.bounds
       self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        self.setNavigationBarItem()
    }

  
  // MARK: UITableView Datasource
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 3
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
    if indexPath.row == 0{
      cell.textLabel?.text = "General Settings"
    }else if indexPath.row == 1{
      cell.textLabel?.text = "Privacy Settings"
    }else{
      cell.textLabel?.text = "Email Notification Settings"
    }
    return cell
    
  }

  // MARK: UITableView Delegate
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row == 0{
      
      var story = UIStoryboard(name: "Main", bundle: nil)
      let vc = story.instantiateViewControllerWithIdentifier("UpdateVC") as! UpdateViewController
      let nav = UINavigationController(rootViewController: vc)
      self.presentViewController(nav, animated: true, completion: nil)
     // self.performSegueWithIdentifier("ModalUpdateVC", sender: self)
    }else if indexPath.row == 1{
      
      var story = UIStoryboard(name: "Main", bundle: nil)
      let vc = story.instantiateViewControllerWithIdentifier("PrivacySettingsVC") as! PrivacyTableViewController
      let nav = UINavigationController(rootViewController: vc)
      self.presentViewController(nav, animated: true, completion: nil)
      
     // self.performSegueWithIdentifier("ModalPrivacyVC", sender: self)
    }else{
      
      var story = UIStoryboard(name: "Main", bundle: nil)
      let vc = story.instantiateViewControllerWithIdentifier("EmailNotificationTableViewController") as! EmailNotificationTableViewController
      let nav = UINavigationController(rootViewController: vc)
      self.presentViewController(nav, animated: true, completion: nil)
  
    }
  }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
