//
//  PersistenceManger.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/4/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class PersistenceManger: NSObject {
    private var allPosts: [PostModel]!
    override init() {
        super.init()
        allPosts = [PostModel]()
        
    }
    
    func saveImage(image: UIImage, filename: String) {
        let path = NSHomeDirectory().stringByAppendingString("/Documents/\(filename)")
        let data = UIImagePNGRepresentation(image)
        data.writeToFile(path, atomically: true)
    }
    
    func getImage(filename: String) -> UIImage? {
        var error: NSError?
        let path = NSHomeDirectory().stringByAppendingString("/Documents/\(filename)")
        let data = NSData(contentsOfFile: path, options: .UncachedRead, error: &error)
        if let unwrappedError = error {
            return nil
        } else {
            return UIImage(data: data!)
        }
    }
    
    func saveAllPosts(allPosts: [PostModel]){
           self.allPosts = allPosts
    }
    
    func getAllPosts()-> [PostModel]{
        return self.allPosts
    }

}
