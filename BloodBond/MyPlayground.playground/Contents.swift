//: Playground - noun: a place where people can play

import UIKit

class Tree: NSObject {
  var name: String?
  var left: Tree?
  var right: Tree?
  init(name: String) {
    super.init()
    self.name = name
   
  }
}

var A = Tree(name: "A")
var B = Tree(name: "B")
var C = Tree(name: "C")
var D = Tree(name: "D")
var E = Tree(name: "E")
var F = Tree(name: "F")
var G = Tree(name: "G")
var H = Tree(name: "H")
var I = Tree(name: "I")


F.left = B
F.right = G

B.left = A
B.right = D
D.left = C
D.right = E

G.right = I
I.left = H



func inorder(){
  
  var current:Tree?
  current = F
  var stack = [Tree]()
  var done = false
  while !done {
    
    if current != nil {
      stack.append(current!)
      current = current?.left
    }else{
      if stack.isEmpty{
        done = true
        print("done")
      }else{
        current = stack.last
        stack.removeLast()
        print(current?.name)
        current = current?.right
      }
    }
    
  }
}

class Me: NSObject {
  var name: String?
  var id: Int?
  init(name: String,id: Int){
    self.name = name
    self.id = id
  }
}

var postTime = "2015-09-05 00:00:00"
let dateFormatter = NSDateFormatter()
dateFormatter.dateStyle =  NSDateFormatterStyle.MediumStyle
dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
let date = dateFormatter.dateFromString(postTime)

var formar = NSDateFormatter()
formar.dateStyle = NSDateFormatterStyle.MediumStyle

let str = formar.stringFromDate(date!)


func postTimeFormatted(postTime: String)-> String{
  // var postTime = "2015-11-03 04:47:12"
  var fsdfsdf = "2015-11-03 04:47:12"
  let dateFormatter = NSDateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
  var date = dateFormatter.dateFromString(fsdfsdf)
  let str = dateFormatter.stringFromDate(date!)

  return dateFormatter.stringFromDate(date!)
}

postTimeFormatted("2015-09-05 00:00:00")
