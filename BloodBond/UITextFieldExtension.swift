//
//  UITextFieldExtension.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 11/1/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import Foundation

extension UITextField{
 func addArrow(){
   var arrow = UIImageView(frame: CGRectMake(CGRectGetWidth(self.frame)-25, 5, 20, 20))
   arrow.image = UIImage(named: "expand_arrow.png")
   self.addSubview(arrow)
  }
}