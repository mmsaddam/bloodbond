//
//  PrivacyTableViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/21/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class PrivacyTableViewController: UITableViewController,UIAlertViewDelegate {
  let CONST_SUCCESS_TAG = 555555
  let CONST_FAIL_TAG = 44444
  
  var  privacy: Privacy!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Privacy Settings"
    self.navigationController?.navigationBar.translucent = false
    
    /**** get privacy ****/
    var dic = [
      "action" : "privacy",
      "user_id" : BloodBond.sharedInstance.loggedUser.user_id
    ]
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.hideHud()
        if success{
          println(JSON)
          self.privacy = self.fetchUserPrivacy(JSON)
          self.tableView.reloadData()
        }else{
          println("fail to fetch privacy")
        }
        
      })
      
    }
    
    
    
    let cancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancel:")
    self.navigationItem.leftBarButtonItem = cancel
    
    let update = UIBarButtonItem(title: "Update", style: .Plain, target: self, action: "update:")
    self.navigationItem.rightBarButtonItem = update
    
    
  }
  
  // MARK: cancel action
  
  @IBAction func cancel(sender: AnyObject) {
    
    self.dismissViewControllerAnimated(true, completion: nil)
    
    
  }
  // MARK: UPDate Acton

  @IBAction func update(sender: AnyObject) {
    var dic = [
      "action": "updatePrivacy",
      "id" : BloodBond.sharedInstance.loggedUser.user_id,
      "email": String(privacy.email),
      "cell" : String(privacy.cell),
      "dob" : String(privacy.dob),
      "location" : String(privacy.location),
      "location2" : "0",
      "job" : String(privacy.job)
    ]
    self.showHud()
    
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.hideHud()
        if success{
          // save changes
          BloodBond.sharedInstance.loggedUser.privacy = self.privacy
          
          /**** Notify pofile that privacy changed ****/
          NSNotificationCenter.defaultCenter().postNotificationName(K_UPDATE_PRIVACY, object: self, userInfo: nil)
          var alert = UIAlertView(title: "Congratulations!", message: "Successfully Updated", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_SUCCESS_TAG
          alert.show()
          
        }else{
          var alert = UIAlertView(title: "Error!", message: "Fail to update", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_FAIL_TAG
          alert.show()
        }
        
      })
      
    }
    
  }
  
  // MARK: UIAlertView Delegate

  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
    if alertView.tag == CONST_SUCCESS_TAG{
      self.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  
  // MARK: fetch user privacy settings data
  func fetchUserPrivacy(json: AnyObject)->Privacy{
    var privacy: Privacy!
    
    if let data: AnyObject = json["data"]{
      var cell  = data.valueForKey("cell") as! Int
      var dob  = data.valueForKey("dob") as! Int
      var email = data.valueForKey("email") as! Int
      var location  = data.valueForKey("location") as! Int
      var job = data.valueForKey("job") as! Int
      privacy = Privacy(cell: cell, dob: dob, email: email, job: job, location: location)
      
    }else{
      println("cann't face privacy settings")
    }
    
    return privacy
  }
  
  
  // MARK: - Table view data source
  
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let sectionHeader = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 10))
    sectionHeader.text = "Display"
    sectionHeader.textAlignment = NSTextAlignment.Right
    sectionHeader.textColor = UIColor.whiteColor()
    sectionHeader.backgroundColor = Color.navyBlue
    return sectionHeader
  }

  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
    if self.privacy != nil{
      return 5
    }else{
      return 0
    }
    
  }
  
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
    cell.accessoryView = nil
    cell.textLabel?.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.9)
    
    switch indexPath.row{
    case 0:
      cell.textLabel?.text = "Email:"
      if privacy.email == 0{
        cell.accessoryView = self.accView()
      }else{
        cell.accessoryType = .Checkmark
        
      }
    case 1:
      cell.textLabel?.text = "Contact Number:"
      if privacy.cell == 0{
        cell.accessoryView = self.accView()
      }else{
        
        cell.accessoryType = .Checkmark
      }
      
    case 2:
      cell.textLabel?.text = "Date of Birth:"
      if privacy.dob == 0{
        
        cell.accessoryView = self.accView()
      }else{
        cell.accessoryType = .Checkmark
      }
    case 3:
      cell.textLabel?.text = "Current Location:"
      if privacy.location == 0{
        cell.accessoryView = self.accView()
      }else{
        cell.accessoryType = .Checkmark
      }
      
    default :
      cell.textLabel?.text = "Job:"
      if privacy.job == 0{
        
        cell.accessoryView = self.accView()
      }else{
        
        cell.accessoryType = .Checkmark
      }
      
    }
    return cell
    
    
  }
  
  // MARK: UITableView Delegate

  override  func  tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    switch indexPath.row{
    case 0:
      if privacy.email == 0{
        privacy.email = 1
      }else{
        privacy.email = 0
      }
    case 1:
      if privacy.cell == 0{
        privacy.cell = 1
      }else{
        privacy.cell = 0
      }
    case 2:
      if privacy.dob == 0{
        privacy.dob = 1
      }else{
        privacy.dob = 0
      }
    case 3:
      if privacy.location == 0{
        privacy.location = 1
      }else{
        privacy.location = 0
      }
      
    default :
      println("default")
      if privacy.job == 0{
        privacy.job = 1
      }else{
        privacy.job = 0
      }
    }
    
    self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
  }
  
  // MARK: Cell Accesory View

  func accView() -> UIImageView{
    var accView = UIImageView(image: UIImage(named: "cross.png"))
    accView.frame = CGRectMake(4, 4, 16, 16)
    return accView
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
}
