//
//  BloodMapViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/15/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AddressBook

class BloodMapViewController: UIViewController {

  var mapDataAry = [MapData]()
  @IBOutlet var bloodMap: MKMapView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false

    self.bloodMap.zoomEnabled = true
    self.bloodMap.frame = self.view.bounds
    // Do any additional setup after loading the view.
    
    for mapData in mapDataAry{
      var geocoder1 = CLGeocoder()
      var address = mapData.location.stringByReplacingOccurrencesOfString(",", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
      geocoder1.geocodeAddressString(address, completionHandler: {(placemarks: [AnyObject]!, error: NSError!) -> Void in
        if let placemark = placemarks?[0] as? CLPlacemark {
          let placeMark = MKPlacemark(placemark: placemark)
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.bloodMap.addAnnotation(MKPlacemark(placemark: placemark))
          })
          
        }
      })
    }
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
