//
//  PostedUserProfileViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/8/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class PostedUserProfileViewController: UIViewController,PostViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate {
  
  @IBOutlet var segmentedControl: UISegmentedControl!
  
  @IBOutlet var tableView: UITableView!
  
  var userId: String!
  var postedUser: User!
  var datePicker: PickerView?
  var profileView: ProfileView?
  var donationCycle: DonaitonCycle?
  var seletedPost: PostModel?
  var allPosts = [PostModel]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    
    self.title = self.postedUser.name
    self.postedUser.user_id = self.userId
    var dic = [
      "action":"userData",
      "user_id":self.userId
    ]
    
    
    self.tableView.backgroundColor = UIColor.clearColor()
    self.tableView.separatorColor = UIColor.clearColor()
    segmentedControl.tintColor = Color.themeColor
    
    /**** tap on tableview cell ****/
    var tap = UITapGestureRecognizer(target: self, action: "handleTap:")
    tap.numberOfTapsRequired = 1
    tap.numberOfTouchesRequired = 1
    tap.delegate = self
    tableView.addGestureRecognizer(tap)
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest("http://dev.bloodbond.co/api", body: dic) { (success, JSON) -> Void in
      
      
      if success == true{
        
        /**** parse JSON and fetch user details ****/
        self.postedUser = BloodBond.sharedInstance.fetchUser(JSON)
        
        /**** send request to check report status ****/
        
        var reportDic = [
          "action" : "getReportStatus",
          "type" : "user",
          "report_to": self.userId,
          "report_by" : BloodBond.sharedInstance.loggedUser.user_id
        ]
        
        
        BloodBond.sharedInstance.postRequest(API_URL, body: reportDic, completion: { (success, JSON) -> Void in
          
          
          if success == true{
            if let status = JSON["getReportStatus"] as? Int{
              if status == 1{
                self.postedUser.reportFlag = true
              }else{
                self.postedUser.reportFlag = false
              }
              
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
              
              self.hideHud()
              self.donationCycleSetup()
              self.userProfileViewSetup()
              self.allPosts = self.postedUser.bloodReq
              self.tableView.dataSource = self
              self.tableView.delegate = self
              self.tableView.reloadData()
              self.tableView.rowHeight = UITableViewAutomaticDimension
              self.tableView.estimatedRowHeight = 160.0
              self.tableView.reloadData()
              
            })
          }else{
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
              let alert = UIAlertView(title: "Ops!!", message: "Please try again", delegate: self, cancelButtonTitle: "Ok")
              alert.tag = 777
              alert.show()
            })
           
          }
          
        })
        
        
        
      }else{
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          let alert = UIAlertView(title: "Ops!!", message: "Please try again", delegate: self, cancelButtonTitle: "Ok")
             alert.tag = 777
          alert.show()
        })
      }
      
    }
    
    
    var leftItem = UIBarButtonItem(image: UIImage(named: "back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "back:")
    leftItem.title = "Back"
    self.navigationItem.leftBarButtonItem = leftItem
    self.navigationController?.navigationBar.translucent = false
    
    self.navigationController?.navigationBar.translucent = false
    
    // Do any additional setup after loading the view.
  }
  
  override  func  viewWillAppear(animated: Bool) {
    
    segmentedControl.selectedSegmentIndex = 0
    
  }
  
  override func viewDidLayoutSubviews() {
  }
  
  
  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
    if alertView.tag == 777{
      self.navigationController?.popViewControllerAnimated(true)
    }
  }
  
  func userProfileViewSetup(){
    profileView = ProfileView(frame: CGRectMake(1.5, 2.5, CGRectGetWidth(self.tableView.frame)-3, 194), user: self.postedUser)
    profileView!.backgroundColor = Color.milkyWhite
    profileView!.postReq.alpha = 0
  }
  
  func donationCycleSetup(){
    self.donationCycle = DonaitonCycle(frame: CGRectMake(1.5, 0, CGRectGetWidth(self.tableView.frame)-3, 135),user: self.postedUser)
    self.donationCycle!.backgroundColor = Color.milkyWhite
    self.donationCycle!.circle.timeLimit = 120
    self.donationCycle!.circle.elapsedTime = NSTimeInterval(self.postedUser.donation.daysSinceDonate!)
    self.donationCycle!.circle.progressLabel.text = String(self.postedUser.donation.daysSinceDonate!)
  }
  
  
  // MARK: UITableView Delegate
  
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
    if section == 0{
      return 194
    }else if section == 1{
      return 135
    }else{
      return 0
    }
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
    return 5.0
    
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
    if section == 0{
      return profileView
    }else if section == 1{
      return donationCycle
    }else{
      if self.allPosts.isEmpty{
        let noPostLabel = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 20))
        noPostLabel.textColor = Color.textColor
        noPostLabel.textAlignment = NSTextAlignment.Center
        noPostLabel.text = "No blood request available"
        return noPostLabel
      }else{
        return nil
      }
      
    }
    
  }
  
  // MARK: UITableView Datasource
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int{
    return 3
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 2{
      return self.allPosts.count
      
    }else{
      return 0
    }
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell: PostCell? = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell
    var post = allPosts[indexPath.row]
    cell?.postTitle.text = post.title
    cell?.postDetails.text = post.details.length >= 100 ? post.details[0...99] + "...continue" : post.details
    cell?.postedBy.text = post.postOwner
    cell?.postDate.text = post.post_time
    cell?.bloodGroup.text = post.bloodGroup
    cell?.border.alpha = indexPath.row == 0 ? 0:1
    
    
    if let img = post.posterImg {
      cell?.PPImageView.image = img
    }else{
      dowloadImage(indexPath) { (image) -> Void in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          cell?.PPImageView?.image = image
          post.posterImg = image
          cell?.indicator.stopAnimating()
        })
      }
    }
    
    return cell!
    
  }
  
  
  func dowloadImage(indexPath: NSIndexPath, completion:(image: UIImage) -> Void){
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
      let link = BloodBond.sharedInstance.getPicUrlFromFbId(self.allPosts[indexPath.row].postOwnerFbId)
      if let url = NSURL(string: link){
        if let data = NSData(contentsOfURL: url){
          if let image = UIImage(data: data){
            completion(image: image)
          }
        }
      }
      
    })
    
  }
  
  
  // MARK: back action
  
  func editPost(sender: UIButton){
    print("fsdfsdfdsf")
  }
  
  func back(sender: UIBarButtonItem){
    self.navigationController?.popViewControllerAnimated(true)
  }
  
  
  
  // MARK: Handle tap
  
  func handleTap(tap: UITapGestureRecognizer){
    if UIGestureRecognizerState.Ended == tap.state {
      var tableView = tap.view as? UITableView
      let point = tap.locationInView(tap.view)
      let indexPath = tableView?.indexPathForRowAtPoint(point)
      let cell = tableView!.cellForRowAtIndexPath(indexPath!) as? PostCell
      let pointInCell = tap.locationInView(cell)
      if CGRectContainsPoint(cell!.postTitle.frame, pointInCell){
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = allPosts[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
      }else if CGRectContainsPoint(cell!.postDetails.frame, pointInCell){
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = allPosts[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
        
        
      }else if CGRectContainsPoint(cell!.postedBy.frame, pointInCell){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var posteUserProfileVC = storyboard.instantiateViewControllerWithIdentifier("PostedUserProfileVC") as! PostedUserProfileViewController
        posteUserProfileVC.postedUser = self.postedUser
        posteUserProfileVC.userId = self.postedUser.user_id
        self.navigationController?.pushViewController(posteUserProfileVC, animated: true)
      }
    }
  }
  
  // MARK: UITapGestureRecognoser Delegate
  
  func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool{
    var tableView = gestureRecognizer.view as? UITableView
    let point = gestureRecognizer.locationInView(gestureRecognizer.view)
    if let isFfound = tableView!.indexPathForRowAtPoint(point){
      return true
    }else{
      return false
    }
  }
  
  
  // MARK: segment action
  @IBAction func segmentedAction(sender: AnyObject) {
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    var segmentedControl = sender as! UISegmentedControl
    var viewController: UIViewController!
    switch segmentedControl.selectedSegmentIndex{
    case 1:
      let viewController = storyboard.instantiateViewControllerWithIdentifier("FriendsVC") as! FriendsViewController
      viewController.user = self.postedUser
      self.navigationController?.pushViewController(viewController, animated: true)
      
    case 2:
      let  viewController = storyboard.instantiateViewControllerWithIdentifier("UserPostDisplayVC") as! UserPostsDisplayViewController
      viewController.allPosts = self.postedUser.myPosts
      self.navigationController?.pushViewController(viewController, animated: true)
      
    case 3:
      let viewController = storyboard.instantiateViewControllerWithIdentifier("BloodMapVC") as! BloodMapViewController
      viewController.mapDataAry = self.postedUser.mapDataAry
      self.navigationController?.pushViewController(viewController, animated: true)
    default: return
    }
    
    
  }
  
  
  func fbPfofilePicUrl(fbId: String)->String{
    var commornUrl = "http://graph.facebook.com/"
    commornUrl  += "\(fbId)"
    commornUrl += "/picture?type=large"
    return commornUrl
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  /*
  
  */
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
