//
//  PostViewDelegate.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/15/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

@objc protocol PostViewDelegate{
  optional func clickOnPostUserName(user: User);
  optional func clickOnPostDetails(post: PostModel);
}