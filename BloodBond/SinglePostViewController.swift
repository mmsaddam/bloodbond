//
//  SinglePostViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/7/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class SinglePostViewController: UIViewController,PostViewDelegate {
  
  
  var post = PostModel()
  
  private var contactDetails: UILabel!
  private var patientsProblem: UILabel!
  private var hostpitalAddress: UILabel!
  private var donationDate: UILabel!
  private var reportPost: UILabel!
  var postView: PostView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Post Details"
    self.navigationController?.navigationBar.translucent = false

    post.showDetails = true
    
    
    var reportDic = [
      "action" : "getReportStatus",
      "type" : "post",
      "report_to":String(self.post.id),
      "report_by" : BloodBond.sharedInstance.loggedUser.user_id
    ]
    
    self.showHud()
    BloodBond.sharedInstance.postRequest(API_URL, body: reportDic, completion: { (success, JSON) -> Void in
      
      if let status = JSON["getReportStatus"] as? Int{
        if status == 1{
          self.post.reportFlag = true
        }else{
          self.post.reportFlag = false
        }
        
      }
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        
        self.hideHud()
        self.postView = PostView(frame: CGRectMake(5, 5, CGRectGetWidth(self.view.frame)-10, 400), post: self.post)
        // postView.delegate = self
        
        self.view.addSubview(self.postView)
        self.setupExraInformation()
        
        var leftItem = UIBarButtonItem(image: UIImage(named: "back.png"), style: UIBarButtonItemStyle.Plain, target: self, action: "back:")
        leftItem.title = "Back"
        self.navigationItem.leftBarButtonItem = leftItem
        self.navigationController?.navigationBar.translucent = false
        
      })
      
      
    })
    
    
    
    // Do any additional setup after loading the view.
  }
  
  override  func  viewDidAppear(animated: Bool) {
    
  }
  
  func back(sender: UIBarButtonItem){
    self.navigationController?.popViewControllerAnimated(true)
  }
  
  // MARK: PostView Delegate
  func clickOnPostUserName(user: User){
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var posteUserProfileVC = storyboard.instantiateViewControllerWithIdentifier("PostedUserProfileVC") as! PostedUserProfileViewController
    posteUserProfileVC.postedUser = user
    posteUserProfileVC.userId = user.user_id
    self.navigationController?.pushViewController(posteUserProfileVC, animated: true)
  }
  
  func clickOnPostDetails(post: PostModel){
    
  }
  
  
  func setupExraInformation(){
    
    var frame = postView.frame
    
    contactDetails = UILabel(frame: CGRectMake(CGRectGetMinX(postView.ppImageView.frame)+5,CGRectGetHeight(postView.frame)+10, CGRectGetWidth(postView.frame)-(CGRectGetMinX(postView.ppImageView.frame)+5), 15))
    contactDetails.textColor = Color.themeColor
    contactDetails.font = UIFont(name: "Helvetica-Bold" , size: FontSize.PostDateFontSize)
    contactDetails.text = "Contact Details: " + post.contact_details
    postView.addSubview(contactDetails)
    
    
    patientsProblem = UILabel()
    patientsProblem.font = UIFont(name: "Helvetica-Bold" , size: FontSize.PostDateFontSize)
    patientsProblem.frame = CGRectMake(CGRectGetMinX(postView.ppImageView.frame)+5 , CGRectGetMaxY(contactDetails.frame)+5, CGRectGetWidth(contactDetails.frame), self.heightForView("Patient's Problem: " + post.patients_problem, view: patientsProblem))
    patientsProblem.textColor = Color.themeColor
    patientsProblem.numberOfLines = 0
    patientsProblem.text = "Patient's Problem: " + post.patients_problem
    patientsProblem.sizeToFit()
    postView.addSubview(patientsProblem)
    
    
    hostpitalAddress = UILabel()
    hostpitalAddress.font = UIFont(name: fontName, size: FontSize.PostDateFontSize)
    hostpitalAddress.frame = CGRectMake(CGRectGetMinX(postView.ppImageView.frame)+5 , CGRectGetMaxY(patientsProblem.frame)+5, CGRectGetWidth(contactDetails.frame), self.heightForView("Patient's Problem: " + post.hospital_address, view: patientsProblem))
    hostpitalAddress.textColor = Color.themeColor
    hostpitalAddress.numberOfLines = 0
    hostpitalAddress.text = "Hospital Address: " + post.hospital_address
    hostpitalAddress.sizeToFit()
    postView.addSubview(hostpitalAddress)
    
    
    donationDate = UILabel()
    donationDate.font = UIFont(name: fontName, size: FontSize.PostDateFontSize)
    
    donationDate.frame = CGRectMake(CGRectGetMinX(postView.ppImageView.frame)+5 , CGRectGetMaxY(hostpitalAddress.frame)+5, CGRectGetWidth(contactDetails.frame), self.heightForView("Patient's Problem: " + post.donate_date, view: patientsProblem))
    donationDate.textColor = Color.themeColor
    donationDate.numberOfLines = 0
    donationDate.text = "Donation Date: " + post.donate_date
    donationDate.sizeToFit()
    postView.addSubview(donationDate)
    
    var reuseFrame = donationDate.frame
    
    reportPost = UILabel()
    reportPost.font = UIFont(name: fontName, size: FontSize.PostDateFontSize)
    reportPost.frame = CGRectMake(CGRectGetMinX(postView.ppImageView.frame)+5 , CGRectGetMaxY(donationDate.frame)+5, CGRectGetWidth(contactDetails.frame),20)
    reportPost.textColor = Color.themeColor
    reportPost.userInteractionEnabled = true
    reportPost.numberOfLines = 0
    reportPost.text = "Report this post"
    reportPost.sizeToFit()
    if self.post.reportFlag == true{
      reportPost.alpha = 0
    }else{
      reuseFrame = reportPost.frame
    }
    postView.addSubview(reportPost)
    
    var shareButton = FBSDKShareButton()
    shareButton.shareContent = facebookShareContent()
    shareButton.frame = CGRectMake(CGRectGetMinX(reuseFrame), CGRectGetMaxY(reuseFrame)+2.5, 55, 14)
    self.postView.addSubview(shareButton)
    
    var tap = UITapGestureRecognizer(target: self, action: "handleTapOnReport:")
    reportPost.addGestureRecognizer(tap)
    
    frame.size.height = CGRectGetMaxY(shareButton.frame)+10
    postView.frame = frame
  }
  
  
  func facebookShareContent()-> FBSDKShareLinkContent{
    
   // println("http://dev.bloodbond.co/images/icons_backup/fbn1_\(self.post.bloodGroup).png")
    var contentUrlStr = COMMON_URL + "post/" + String(self.post.id)
    var contentURL = NSURL(string: contentUrlStr)
    
  //  var contentURL = NSURL(string: "http://dev.bloodbond.co/post/\(self.post.id)")
    var imageUrlStr = COMMON_URL + "/images/icons_backup/fbn1_" + self.post.bloodGroup.lowercaseString + ".png"
    var imageURL =  NSURL(string: imageUrlStr)
    var contentTitle = self.post.title
    var contentDescription = self.post.details
    var shareContent = FBSDKShareLinkContent()
    shareContent.imageURL = imageURL
    shareContent.contentURL = contentURL
    shareContent.contentTitle = contentTitle
    shareContent.contentDescription = contentDescription
    
    return shareContent
  }

  
  
  func handleTapOnReport(tap: UITapGestureRecognizer){
    var alert = UIAlertView()
    alert.title = "Confirmation"
    alert.message = "Are you sure?"
    alert.delegate = self
    alert.addButtonWithTitle("No")
    alert.addButtonWithTitle("Ok")
    alert.show()
    
  }
  
  func alertView(alertView: UIAlertView!, clickedButtonAtIndex buttonIndex: Int){
    if buttonIndex == 1{
      
      var reportDic = [
        "action" : "report",
        "type" : "post",
        "report_to": String(post.id),
        "report_by" : BloodBond.sharedInstance.loggedUser.user_id
      ]
      self.showHud()
      BloodBond.sharedInstance.postRequest(API_URL, body: reportDic, completion: { (success, JSON) -> Void in
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          self.hideHud()
          
          if let status = JSON["report"] as? Int{
            if status == 1{
              self.reportPost.removeFromSuperview()
            }else{
              let alert = UIAlertView(title: "Ops!", message: "Fail to report", delegate: nil, cancelButtonTitle: "Ok")
              alert.show()
            }
            
          }
        })
        
      })
      
      
    }
    
    
  }
  
  func heightForView(text:String, view: UILabel) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRectMake(0, 0, view.frame.width, CGFloat.max))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.ByWordWrapping
    label.font = view.font
    label.text = text
    
    label.sizeToFit()
    return label.frame.height
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
