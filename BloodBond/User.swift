//
//  User.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/8/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class User: NSObject {
  var isLoggedIn = false
  var user_id: String!
  var fbID: String!
  var name: String!
  var gender: String!
  var birthday: String!
  var work: String!
  var location: String!
  var hometown: String!
  var email: String!
  var mobile = ""
  var fbProfileLink: String!
  // var picture: UIImage!
  var pictureUrl: String!
  var curLocCor: Cordinate!
  var homeLocCor: Cordinate!
  var donation: Donation!
  var privacy: Privacy!
  var bloodGroup: String!
  var mapDataAry: [MapData]!
  var myPosts: [PostModel]!
  var bloodReq: [PostModel]!
  var reportFlag = false

    override init() {
        super.init()
    }
}
