//
//  PickerView.swift
//  AppBanking
//
//  Created by Muzahidul Islam on 4/29/15.
//  Copyright (c) 2015 DIGI-TECH Bangladesh LTD. All rights reserved.
//

import UIKit

class PickerView: UIView {
    var datePicker : UIDatePicker!
    var done : UIButton!

    init(kframe: CGRect) {
        super.init(frame:kframe)
        self.backgroundColor = Color.themeColor
        self.layer.cornerRadius = 7
        datePicker = UIDatePicker()
        datePicker.maximumDate = NSDate()
        datePicker.setValue(UIColor.whiteColor(), forKey: "textColor")
        datePicker.datePickerMode = .Date
        datePicker.frame = CGRectMake(0, 0, CGRectGetWidth(kframe), CGRectGetHeight(kframe)-45)
        self.addSubview(datePicker)
        
        done = UIButton(frame: CGRectMake((CGRectGetWidth(kframe)-90)/2, CGRectGetMaxY(datePicker.frame), 90, 30))
        done.setTitle("Done", forState: UIControlState.Normal)
        done.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        done.layer.cornerRadius = 7
        done.backgroundColor = UIColor.whiteColor()
        self.addSubview(done)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func date() -> NSDate{
        return datePicker.date
    }

}
