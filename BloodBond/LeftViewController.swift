//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case Home = 0
    case Profile
    case Settings
    case Logout
    case NonMenu
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Home", "Profile","Settings","Logout"]
    var mainViewController: UIViewController!
    var profileViewController: UIViewController!
    var settingsViewController: UIViewController!
    var logoutViewController: UIViewController!
   
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let profileViewController = storyboard.instantiateViewControllerWithIdentifier("ProfileViewController") as! ProfileViewController
        self.profileViewController = UINavigationController(rootViewController: profileViewController)
        
        let settingsViewController = storyboard.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
        self.settingsViewController = UINavigationController(rootViewController: settingsViewController)
        
        let logoutViewController = storyboard.instantiateViewControllerWithIdentifier("LogoutViewController") as! LogoutViewController
        self.logoutViewController = UINavigationController(rootViewController: logoutViewController)
        
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: BaseTableViewCell = BaseTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: BaseTableViewCell.identifier)
        cell.backgroundColor = Color.themeColor
        cell.textLabel?.font = UIFont.italicSystemFontOfSize(18)
        cell.textLabel?.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        cell.textLabel?.text = menus[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu)
        }
    }
    
    func changeViewController(menu: LeftMenu) {
        switch menu {
        case .Home:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Profile:
            self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
            break
        case .Settings:
            self.slideMenuController()?.changeMainViewController(self.settingsViewController, close: true)
        case .Logout:
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.logout()
           break;
        default:
            break
        }
    }
    
       
}