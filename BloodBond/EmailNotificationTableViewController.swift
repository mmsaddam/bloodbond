//
//  EmailNotificationTableViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/21/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class EmailNotificationTableViewController: UITableViewController,UIAlertViewDelegate {
  let CONST_SUCCESS_TAG = 555555
  let CONST_FAIL_TAG = 444444
  
  var check: Int?
  var  privacy: Privacy!
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    self.title = "Email Settings"
    
    /**** get privacy ****/
    var dic = [
      "action" : "privacy",
      "user_id" : BloodBond.sharedInstance.loggedUser.user_id
    ]
    
    self.showHud()
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.hideHud()
        if success{
          println(JSON)
          self.privacy = self.fetchUserPrivacy(JSON)
          self.check = self.privacy.IsMail
          self.tableView.reloadData()
        }else{
          println("fail to fetch privacy")
        }
        
      })
      
    }
    
    
    let cancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancel:")
    self.navigationItem.leftBarButtonItem = cancel
    
    let update = UIBarButtonItem(title: "Update", style: .Plain, target: self, action: "update:")
    self.navigationItem.rightBarButtonItem = update
    
  }
  
  // MARK: fetch user privacy settings data
  func fetchUserPrivacy(json: AnyObject)->Privacy{
    var privacy: Privacy!
    
    if let data: AnyObject = json["data"]{
      var cell  = data.valueForKey("cell") as! Int
      var dob  = data.valueForKey("dob") as! Int
      var email = data.valueForKey("email") as! Int
      var location  = data.valueForKey("location") as! Int
      var job = data.valueForKey("job") as! Int
      privacy = Privacy(cell: cell, dob: dob, email: email, job: job, location: location)
      privacy.IsMail =  data.valueForKey("isEmail") as! Int
      
    }else{
      println("cann't face privacy settings")
    }
    
    return privacy
  }
  
  // MARK: UPDate Action

  @IBAction func update(sender: AnyObject) {
    
    var dic = [
      "action" : "emailNotify",
      "id" : BloodBond.sharedInstance.loggedUser.user_id,
      "notification" : String(check!)
    ]
    
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.hideHud()
        if success{
          println(JSON)
          BloodBond.sharedInstance.loggedUser.privacy = self.privacy
          var alert = UIAlertView(title: "Congratulations!", message: "Successfully Updated", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_SUCCESS_TAG
          alert.show()
          
        }else{
          var alert = UIAlertView(title: "Error!", message: "Fail to update", delegate: self, cancelButtonTitle: "Ok")
          alert.tag = self.CONST_FAIL_TAG
          alert.show()
        }
        
      })
      
    }
    
    
  }
  
  // MARK: UIAlertView Delegate

  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
    if alertView.tag == CONST_SUCCESS_TAG{
      self.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  // MARK: cancel action

  @IBAction func cancel(sender: AnyObject) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let val = check {
      return 4
    }else{
      return 0
    }
  }
  
  override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let sectionHeader = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 10))
    sectionHeader.text = "Receive email of posts"
    sectionHeader.textColor = UIColor.whiteColor()
    sectionHeader.textAlignment = NSTextAlignment.Center
    sectionHeader.backgroundColor = Color.navyBlue
    return sectionHeader
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
    cell.accessoryView = nil
    cell.textLabel?.font = UIFont.systemFontOfSize(13)
    cell.textLabel?.adjustsFontSizeToFitWidth = true
    cell.textLabel?.sizeToFit()
    cell.textLabel?.numberOfLines = 0
    cell.textLabel?.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.9)
    switch indexPath.row{
    case 0: cell.textLabel?.text = "Related to my blood group and posted by my friends"
    case 1: cell.textLabel?.text = "Related to any blood group posted by my friends"
    case 2: cell.textLabel?.text = "Related to my blood group"
    default: cell.textLabel?.text = "Related to any blood group"
    }
    
    if let check = check where check == indexPath.row {
      cell.accessoryType = .Checkmark
      
    }else{
      cell.accessoryView = self.accView()
    }
    
    return cell
  }
  
  
  override  func  tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    check = indexPath.row
    self.tableView.reloadData()
  }
  
  // MARK: accessory view for cell
  func accView() -> UIImageView{
    var accView = UIImageView(image: UIImage(named: "cross.png"))
    accView.frame = CGRectMake(4, 4, 16, 16)
    return accView
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
  }
  */
  
}
