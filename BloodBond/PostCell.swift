//
//  PostCell.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/26/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

  @IBOutlet var PPImageView: UIImageView!
  @IBOutlet var bloodGroup: UILabel!
  @IBOutlet var postDate: UILabel!
  @IBOutlet var postedBy: UILabel!
  @IBOutlet var postTitle: UILabel!
  @IBOutlet var postDetails: UILabel!
  @IBOutlet var editButton: UIButton!
  var border: UILabel!
  var indicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      setup()
    }

  func setup(){
    
   // self.contentView.backgroundColor = UIColor.clearColor()
    indicator = UIActivityIndicatorView()
    indicator.activityIndicatorViewStyle = .Gray
    indicator.center = PPImageView.center
    indicator.startAnimating()
    self.addSubview(indicator)
    PPImageView.layer.cornerRadius = CGRectGetWidth(PPImageView.frame)/2
    PPImageView.layer.borderWidth = 4
    PPImageView.layer.borderColor = UIColor.whiteColor().CGColor
    PPImageView.clipsToBounds = true
    postTitle.textColor = Color.textColor
    postDetails.textColor = Color.textColor
    postDate.textColor = Color.postDateColor
    postedBy.textColor = Color.themeColor
    bloodGroup.textColor = Color.themeColor
    bloodGroup.layer.borderColor = Color.themeColor.CGColor
    bloodGroup.layer.borderWidth = 2.0
    bloodGroup.layer.cornerRadius = 7
    postDate.font = UIFont.systemFontOfSize(FontSize.PostDateFontSize)
    postedBy.font = UIFont.systemFontOfSize(FontSize.PostedUserFontSize)
    postTitle.font = UIFont.systemFontOfSize(FontSize.PostTitleFontSize)
    postDetails.font = UIFont.systemFontOfSize(FontSize.PostDetailsFontSize)
    border = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame), 5))
    border.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
    self.addSubview(border)

    }
  
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
