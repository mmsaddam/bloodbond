//
//  ProfileViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
  
  @IBOutlet var segmentedControl: UISegmentedControl!
  @IBOutlet var tableView: UITableView!
  
  var loggedUser: User!
  var datePicker: PickerView!
  var donationCycle: DonaitonCycle?
  var profileView: ProfileView?
  var allPostsReq = [PostModel]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    self.title = "Profile"
    
    loggedUser = BloodBond.sharedInstance.loggedUser
    
    self.tableView.backgroundColor = UIColor.clearColor()
    self.tableView.separatorColor = UIColor.clearColor()
    self.tableView.dataSource = self
    self.tableView.delegate = self
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.estimatedRowHeight = 160.0
    segmentedControl.tintColor = Color.themeColor
    
    /**** tap on tableview cell ****/
    var tap = UITapGestureRecognizer(target: self, action: "handleTap:")
    tap.numberOfTapsRequired = 1
    tap.numberOfTouchesRequired = 1
    tap.delegate = self
    tableView.addGestureRecognizer(tap)
    
    self.reloadScroller()
    
    datePicker = PickerView(kframe: CGRectMake(15, CGRectGetHeight(self.view.frame)/2-100, CGRectGetWidth(self.view.frame)-30, 200))
    datePicker.done.addTarget(self, action: "datePickerDone", forControlEvents: UIControlEvents.TouchUpInside)
    datePicker.alpha = 0
    self.view.addSubview(datePicker)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePost:", name: K_UPDATE_POST, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "rePost:", name: K_RE_POST, object: nil)

    NSNotificationCenter.defaultCenter().addObserver(self, selector:"newPost:", name: K_NewPost, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"deletePost:", name: K_DELETE_POST, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"updatePrivacy:", name: K_UPDATE_PRIVACY, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"updateGeneralSettings:", name: K_UPDATE_GENERAL_SETTINGS, object: nil)
    
  }
  
  
  
  override  func  viewDidAppear(animated: Bool) {
    self.navigationController?.navigationBar.translucent = false
  }
  
  override func viewWillAppear(animated: Bool) {
    segmentedControl.selectedSegmentIndex = 0
    self.setNavigationBarItem()
  }
  
  
  
  override func viewDidLayoutSubviews() {
  ///  self.segmentedControl.frame = CGRectMake(0, 1, CGRectGetWidth(self.view.frame), 30)
  }
  
  
  func reloadScroller(){

    var dic = [
      "action":"userData",
      "user_id":self.loggedUser.user_id
    ]
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        
        self.hideHud()
        
        if success == true{
          /**** parse JSON and fetch user details ****/
          self.loggedUser = BloodBond.sharedInstance.fetchUser(JSON)
          self.loggedUser.isLoggedIn = true
          
          self.setupProfileView()
          self.setUpDonationCycleView()
          
          self.allPostsReq = self.loggedUser.bloodReq
          self.tableView.reloadData()
        }else{
          self.reloadScroller()
        }
        
        
   
      })
      
      
    }
    
   
    
    
  }
  
  // MARK: Profile View
  
  func setupProfileView(){
    profileView = ProfileView(frame: CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 170), user: self.loggedUser)
    
    self.profileView!.postReq.addTarget(self, action: "postReq:", forControlEvents: UIControlEvents.TouchUpInside)
    self.profileView!.backgroundColor = Color.milkyWhite
  }

  // MARK: Donation Cycle View
  func setUpDonationCycleView(){
    self.donationCycle = DonaitonCycle(frame: CGRectMake(0,0, CGRectGetWidth(self.view.frame), 180),user: self.loggedUser)
    self.donationCycle!.circle.timeLimit = 120
    self.donationCycle!.circle.elapsedTime = NSTimeInterval(self.loggedUser.donation.daysSinceDonate!)
    self.donationCycle!.daysLabel.text = String(self.loggedUser.donation.daysSinceDonate!)
    self.donationCycle!.donateBtn.addTarget(self, action: "lastDonate:", forControlEvents: UIControlEvents.TouchUpInside)
  }

  // MARK: Notificaton Hadle
  
  func updatePrivacy(notification: NSNotification){
    
    self.loggedUser.privacy = BloodBond.sharedInstance.loggedUser.privacy
    setupProfileView()
    self.tableView.reloadData()

    
  }
  
  func updateGeneralSettings(notification: NSNotification){
    self.reloadScroller()
    
  }
 
  
  func deletePost(notification: NSNotification) {
    
    let userInfo = notification.userInfo as! [String: AnyObject]
    var deletedPost = userInfo["POST"] as! PostModel?
    
    if let deletedPost = userInfo["POST"] as? PostModel{
       println("delted post \(deletedPost.id)")
      for (index, element) in enumerate(self.loggedUser.myPosts) {
        if element.id == deletedPost.id{
          self.loggedUser.myPosts.removeAtIndex(index)
          break;
        }
      }
      
      for (index, element) in enumerate(self.loggedUser.bloodReq) {
        if element.id == deletedPost.id{
          self.loggedUser.bloodReq.removeAtIndex(index)
          break;
        }
      }
    }
    
   self.allPostsReq = self.loggedUser.bloodReq
   self.tableView.reloadData()
    
  }
  
  func updatePost(notification: NSNotification) {
    
     self.reloadScroller()
    
  }

  func rePost(notification: NSNotification) {
    
    reloadScroller()
    
  }
  
  func newPost(notification: NSNotification) {
    
    reloadScroller()
    
  }
  
  
  
  // MARK: UITableView Delegate
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
    if section == 0{
      return 170
    }else if section == 1{
      return 180
    }else{
      if self.allPostsReq.isEmpty{
        return 20
      }else{
        return UITableViewAutomaticDimension
      }
    }
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
   return 5.0
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
    if section == 0{
          return profileView
      
    }else if section == 1{
      
            return donationCycle
      
    }else{
       if self.allPostsReq.isEmpty{
        let noPostLabel = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 20))
        noPostLabel.textColor = Color.textColor
        noPostLabel.textAlignment = NSTextAlignment.Center
        noPostLabel.text = "No blood request available"
        return noPostLabel
      }else{
        return nil
      }

    }
    
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.section == 2{
      
    }
  }
  
  // MARK: UITableView Datasource
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int{
    return 3
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 2{
      
      return self.allPostsReq.count
      
    }else{
      return 0
    }
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell: PostCell? = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell
    var post = allPostsReq[indexPath.row]
    cell?.postTitle.text = post.title
    cell?.postDetails.text = post.details.length >= 100 ? post.details[0...99] + "...continue" : post.details
    cell?.postedBy.text = post.postOwner
    cell?.postDate.text = post.post_time
    cell?.bloodGroup.text = post.bloodGroup
    cell?.border.alpha = indexPath.row == 0 ? 0:1
    
    
    if let img = post.posterImg {
      cell?.PPImageView.image = img
    }else{
      dowloadImage(indexPath) { (image) -> Void in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          cell?.PPImageView?.image = image
          post.posterImg = image
          cell?.indicator.stopAnimating()
        })
      }
    }
    
    return cell!
    
  }
  
  func dowloadImage(indexPath: NSIndexPath, completion:(image: UIImage) -> Void){
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
      let link = BloodBond.sharedInstance.getPicUrlFromFbId(self.allPostsReq[indexPath.row].postOwnerFbId)
      if let url = NSURL(string: link){
        if let data = NSData(contentsOfURL: url){
          if let image = UIImage(data: data){
            completion(image: image)
          }
        }
      }
      
    })
    
  }
  
  
  
  // MARK: button action
  func postReq(sender: UIButton){
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let postReqVC = storyBoard.instantiateViewControllerWithIdentifier("MakePostVC") as! MakePostViewController
    postReqVC.flag = 1
    let nav = UINavigationController(rootViewController: postReqVC)
    self.presentViewController(nav, animated: true, completion: nil)
  }
  
  func lastDonate(sender: UIButton){
    self.view.bringSubviewToFront(datePicker)
    datePicker.datePicker.date = NSDate()
    datePicker.datePicker.maximumDate = NSDate()
    datePicker.showMe()
  }
  
  
  
  func datePickerDone(){
    
    BloodBond.sharedInstance.donationSession.finishDate = datePicker.date()
    
    let formatter = NSDateFormatter()
    formatter.dateFormat = "MM/dd/yyyy"
    
    var dic =
    ["action" : "saveDonate",
      "user_id" : BloodBond.sharedInstance.loggedUser.user_id,
      "donate_date" : formatter.stringFromDate(datePicker.date())
    ]
    self.datePicker.hideMe()
    self.showHud()
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        self.hideHud()
        if let status: Int  =  JSON["saveDonate"] as? Int where status == 1{
          
          self.updateDonation(self.datePicker.date())
          
        }else{
          println("fail to update donate")
        }
        
        
      })
    }
    
    
    //  println(NSDate().daysFrom(datePicker.date()))
    
  }
  
  func updateDonation(date: NSDate){
    
    var lastDonateDate: String!
    var nextDonateDate: String
    var totalDonate: Int!
    var daysSinceDonate: Int!
    
    var last = date
    var next = date.dateByAddingTimeInterval(60*60*24*120)
    donationCycle!.lastBloodDoante(last)
    donationCycle!.canDonateFrom(next)
    
    var days =  NSDate().daysFrom(date)
    
    if days>=120{
      days = 120
    }
    
    
    totalDonate = loggedUser.donation.totalDonate
    totalDonate = totalDonate + 1
    loggedUser.donation.totalDonate = totalDonate
    donationCycle!.bloodDonate.text = "Total Donate: " + String(totalDonate)
    //    var donate = Donation(last: "\(last)", next: "\(next)", totalDonate: totalDonate, daysSinceDonate: days)
    //    BloodBond.sharedInstance.loggedUser.donation = donate
    
    //  donationCycle.bloodDonate.text = "Total Donate: " + String(totalDonate)
    // donationCycle.numberOfTimeDoate(4)
    donationCycle!.circle.elapsedTime = NSTimeInterval(days)
    donationCycle!.daysLabel.text = String(days)
    
  }
  
  
  @IBAction func segmentedControlAction(sender: AnyObject) {
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    var segmentedControl = sender as! UISegmentedControl
    var viewController: UIViewController!
    switch segmentedControl.selectedSegmentIndex{
      
    case 1:
      let viewController = storyboard.instantiateViewControllerWithIdentifier("FriendsVC") as! FriendsViewController
      viewController.user = self.loggedUser
      self.navigationController?.pushViewController(viewController, animated: true)
      
    case 2:
      let  viewController = storyboard.instantiateViewControllerWithIdentifier("UserPostDisplayVC") as! UserPostsDisplayViewController
      viewController.allPosts = self.loggedUser.myPosts
      self.navigationController?.pushViewController(viewController, animated: true)
      
    case 3:
      let viewController = storyboard.instantiateViewControllerWithIdentifier("BloodMapVC") as! BloodMapViewController
      viewController.mapDataAry = self.loggedUser.mapDataAry
      self.navigationController?.pushViewController(viewController, animated: true)
    default: return
    }
    
  }
  
  
  
  // MARK: Handle tap
  
  func handleTap(tap: UITapGestureRecognizer){
    if UIGestureRecognizerState.Ended == tap.state {
      var tableView = tap.view as? UITableView
      let point = tap.locationInView(tap.view)
      let indexPath = tableView?.indexPathForRowAtPoint(point)
      let cell = tableView!.cellForRowAtIndexPath(indexPath!) as? PostCell
      let pointInCell = tap.locationInView(cell)
      if CGRectContainsPoint(cell!.postTitle.frame, pointInCell){
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = allPostsReq[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
      }else if CGRectContainsPoint(cell!.postDetails.frame, pointInCell){
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = allPostsReq[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
        
        
      }else if CGRectContainsPoint(cell!.postedBy.frame, pointInCell){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var posteUserProfileVC = storyboard.instantiateViewControllerWithIdentifier("PostedUserProfileVC") as! PostedUserProfileViewController
        posteUserProfileVC.postedUser = self.loggedUser
        posteUserProfileVC.userId = self.loggedUser.user_id
      }
      
    }
  }
  
  // MARK: UITapGestureRecognoser Delegate
  
  func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool{
    var tableView = gestureRecognizer.view as? UITableView
    let point = gestureRecognizer.locationInView(gestureRecognizer.view)
    if let isFfound = tableView!.indexPathForRowAtPoint(point){
      return true
    }else{
      return false
    }
  }

  
  /** make facebook url from facebook id **/
  func fbPfofilePicUrl(fbId: String)->String{
    var commornUrl = "http://graph.facebook.com/"
    commornUrl  += "\(fbId)"
    commornUrl += "/picture?type=large"
    return commornUrl
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  deinit{
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
