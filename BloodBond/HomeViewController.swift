//
//  HomeViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate {
  
  enum BloodGroup: Int{
    case All = 0
    case AN   // A-
    case AP    // A+
    case BN     //B-
    case BP     // B+
    case ON    // O-
    case OP  // O+
    case ABN   // AB-
    case ABP   // AB+
  }
  
  var loggedUser: User!
  // var postModelArray = [PostModel]()
  var allPosts = [PostModel]()
  var tempPost = [PostModel]()
  var postDic = [String:[PostModel]]()
  var seletedPost: PostModel!
  let cellIdentifier = "PostCell"
  
  var bloodGroup = BloodGroup.All
  
  let HUD = MBProgressHUD()
  
  @IBOutlet var segmentedControl: UISegmentedControl!
  
  @IBOutlet var tableView: UITableView!
  // MARK: view cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    self.title = "Home"
    
    reloadScroller()
   
    tableView.dataSource = self
    tableView.delegate = self
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.separatorColor = UIColor.clearColor()
    self.tableView.estimatedRowHeight = 160.0
    
    segmentedControl.tintColor = Color.themeColor
    
    /**** tap on tableview cell ****/
    var tap = UITapGestureRecognizer(target: self, action: "handleTap:")
    tap.numberOfTapsRequired = 1
    tap.numberOfTouchesRequired = 1
    tap.delegate = self
    tableView.addGestureRecognizer(tap)
    
    /**** Notification Observer ****/
//    NSNotificationCenter.defaultCenter().addObserver(self, selector:"newPost:", name: K_NewPost, object: nil)
//    NSNotificationCenter.defaultCenter().addObserver(self, selector:"rePost:", name: K_RE_POST, object: nil)
//    NSNotificationCenter.defaultCenter().addObserver(self, selector:"deletePost:", name: K_DELETE_POST, object: nil)
//    NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePost:", name: K_UPDATE_POST, object: nil)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"handleNotification:", name: K_NewPost, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"handleNotification:", name: K_RE_POST, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector:"handleNotification:", name: K_DELETE_POST, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleNotification:", name: K_UPDATE_POST, object: nil)
    
    
    
  }
  

  override func viewWillAppear(animated: Bool) {
    // set left navigation menu
    self.setNavigationBarItem()
    
  }

  
  // MARK: Segmented Action
  @IBAction func segmentedAction(sender: AnyObject) {
//    case All = 0
//    case AN   // A-
//    case AP    // A+
//    case BP     //B+
//    case BN     // B-
//    case OP    // O+
//    case ON  //
//    case ABP   // AB+
//    case ABN   // AB-

    var segmentedControl = sender as! UISegmentedControl
    switch segmentedControl.selectedSegmentIndex{
    case 0: self.bloodGroup = BloodGroup.All
    case 1: self.bloodGroup = BloodGroup.AN
    case 2: self.bloodGroup = BloodGroup.AP
    case 3: self.bloodGroup = BloodGroup.BN
    case 4: self.bloodGroup = BloodGroup.BP
    case 5: self.bloodGroup = BloodGroup.ON
    case 6: self.bloodGroup = BloodGroup.OP
    case 7: self.bloodGroup = BloodGroup.ABN
    default: self.bloodGroup = BloodGroup.ABP
    }
    self.tempPost = postByCatagory()
    self.tableView.reloadData()
  }
  

  /** search post by cartagory **/
  func postByCatagory()-> [PostModel]{
    var posts = [PostModel]()
    switch self.bloodGroup{
    case .All: posts = self.allPosts
    case .AN: posts = filterPost("A-")
    case .AP: posts = filterPost("A+")
    case .BN: posts = filterPost("B-")
    case .BP: posts = filterPost("B+")
    case .ON: posts = filterPost("O-")
    case .OP: posts = filterPost("O+")
    case .ABN: posts = filterPost("AB-")
    case .ABP: posts = filterPost("AB+")
    }
    return posts
  }
  
  func filterPost(key: String)-> [PostModel]{
    var tmp = [PostModel]()
    for item in allPosts{
      if item.bloodGroup == key{
        tmp.append(item)
      }
    }
    return tmp
  }
  
  // MARK: Notification Handle
  func handleNotification(notification: NSNotification) {
    
    switch notification.name{
    case K_DELETE_POST:
      let userInfo = notification.userInfo as! [String: AnyObject]
      var deletedPost = userInfo["POST"] as! PostModel?
      if let deletedPost = userInfo["POST"] as? PostModel{
        //  println("delted post \(deletedPost.id)")
        for (index, element) in enumerate(self.allPosts) {
          if element.id == deletedPost.id{
            self.allPosts.removeAtIndex(index)
            break;
          }
        }
      }
      self.tableView.reloadData()
      
    case K_NewPost: reloadScroller()
    case K_UPDATE_POST: reloadScroller()
    case K_RE_POST: reloadScroller()
    default: reloadScroller()
    }
    
  }

  
  func reloadScroller(){
    
    loggedUser = BloodBond.sharedInstance.loggedUser
    
    var dic = [
      "action":"getAllPost",
      "user_id":"\(loggedUser.user_id)"
    ]
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, JSON) -> Void in
       println(JSON)
      if success == true { // successfully found data
        if let dataArray: [AnyObject] = JSON["data"] as? Array{
          
          self.allPosts = [] // empty array
          //self.allPosts.removeAll(keepCapacity: false)
          
          for dataDic in dataArray{
            self.allPosts.append(self.preparePostModel(dataDic))
          }
          
          
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tempPost = []
            self.tempPost = self.allPosts
            self.tableView.reloadData()
            self.hideHud()
          })
          
          // save the all post to shared intance class
          //   BloodBond.sharedInstance.saveAllPosts(self.postModelArray)
          
        }else{
          
          println("data not found")
        }
        
      }else{
        
        println("unable to fetch data")
      }
      
    }
    
  }
  
  // MARK: Handle tap
  
  func handleTap(tap: UITapGestureRecognizer){
    if UIGestureRecognizerState.Ended == tap.state {
      var tableView = tap.view as? UITableView
      let point = tap.locationInView(tap.view)
      let indexPath = tableView?.indexPathForRowAtPoint(point)
      let cell = tableView!.cellForRowAtIndexPath(indexPath!) as? PostCell
      
      let pointInCell = tap.locationInView(cell)
      if CGRectContainsPoint(cell!.postTitle.frame, pointInCell){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = self.tempPost[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
      }else if CGRectContainsPoint(cell!.postDetails.frame, pointInCell){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = self.tempPost[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
      }else if CGRectContainsPoint(cell!.postedBy.frame, pointInCell){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var posteUserProfileVC = storyboard.instantiateViewControllerWithIdentifier("PostedUserProfileVC") as! PostedUserProfileViewController
        var user = self.makePostedUserProfile(self.tempPost[indexPath!.row])
        posteUserProfileVC.postedUser = user
        posteUserProfileVC.userId = user.user_id
        self.navigationController?.pushViewController(posteUserProfileVC, animated: true)
      }
      
    }
  }
  
  // MARK: UITapGestureRecognoser Delegate
  
  func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool{
    var tableView = gestureRecognizer.view as? UITableView
    let point = gestureRecognizer.locationInView(gestureRecognizer.view)
    if let isFfound = tableView!.indexPathForRowAtPoint(point){
      return true
    }else{
      return false
    }
  }
  
  // MARK: UITableView Datasource
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
    if self.tempPost.isEmpty{
      return 20
    }else{
      return UITableViewAutomaticDimension
    }
    
  }
  
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
    if section == 0{
      if self.tempPost.isEmpty{
        let noPostLabel = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 20))
        noPostLabel.textColor = Color.textColor
        noPostLabel.textAlignment = NSTextAlignment.Center
        noPostLabel.text = "No post available"
        return noPostLabel
      }else{
        return nil
      }
      
    }else{
      return nil
    }
    
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.tempPost.count
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell: PostCell? = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell
    
    var post = tempPost[indexPath.row]
    cell?.postTitle.text = post.title
    cell?.postDetails.text = post.details.length >= 100 ? post.details[0...99] + "...continue" : post.details
    cell?.postedBy.text = post.postOwner
    cell?.postDate.text = post.post_time
    cell?.bloodGroup.text = post.bloodGroup
    cell?.border.alpha = indexPath.row == 0 ? 0:1
    
    if let img = post.posterImg {
      cell?.PPImageView.image = img
    }else{
      dowloadImage(indexPath) { (image) -> Void in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          cell?.PPImageView?.image = image
          post.posterImg = image
          cell?.indicator.stopAnimating()
        })
      }
    }
    return cell!
  }
  
  func editPost(sender: UIButton){
    
  }
  
  // MARK: ScrollView Delegate
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    let offset = scrollView.contentOffset
    let size = scrollView.contentSize
    let bound = scrollView.bounds
    let inset = scrollView.contentInset
    let y = offset.y + bound.size.height - inset.bottom
    let h = size.height
    let reload_distance: CGFloat = 10
    let total = h + reload_distance
    if y > total {
      print("end .....")
    }
    
  }
  
  func dowloadImage(indexPath: NSIndexPath, completion:(image: UIImage) -> Void){
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
      let link = BloodBond.sharedInstance.getPicUrlFromFbId(self.tempPost[indexPath.row].postOwnerFbId)
      if let url = NSURL(string: link){
        if let data = NSData(contentsOfURL: url){
          if let image = UIImage(data: data){
            completion(image: image)
          }
        }
      }
      
    })
    
  }
  
  
  // MARK: PostView Delegate
  func clickOnPostUserName(user: User){
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var posteUserProfileVC = storyboard.instantiateViewControllerWithIdentifier("PostedUserProfileVC") as! PostedUserProfileViewController
    posteUserProfileVC.postedUser = user
    posteUserProfileVC.userId = user.user_id
    self.navigationController?.pushViewController(posteUserProfileVC, animated: true)
  }
  
  func clickOnPostDetails(post: PostModel){
    var storyboard = UIStoryboard(name: "Main", bundle: nil)
    var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
    singlePostVC.post = post
    self.navigationController?.pushViewController(singlePostVC, animated: true)
    
  }
  
  
  
  func makePostedUserProfile(post: PostModel)->User{
    var user = User()
    user.fbID = "\(post.postOwnerFbId)"
    user.gender = "loading..."
    user.name = post.postOwner
    user.hometown = "loading..."
    user.location = "loading..."
    user.email = "loading..."
    user.birthday = "loading"
    user.user_id = String(post.user_id)
    user.pictureUrl = BloodBond.sharedInstance.getPicUrlFromFbId(post.postOwnerFbId)
    return user
    
  }
  
  
  /* Prepare Post Model class from JSON Dic */
  
  func preparePostModel(dataDic: AnyObject)-> PostModel{
    
    var postOwner = dataDic["name"] as! String
    var postOwnerFbID: AnyObject = dataDic["fbid"]!!
    var bloodGroup  = dataDic["blood_group"] as! Int
    var contact_details = dataDic["contact_details"] as! String
    var created_at = dataDic["created_at"] as! String
    var flag = dataDic["flag"] as! Int
    var hospital_address = dataDic["hospital_address"] as! String
    var id = dataDic["id"] as! Int
    var isDelete = dataDic["isDelete"] as! Int
    var patients_problem = dataDic["patients_problem"] as! String
    var post_time = dataDic["post_time"] as! String
    var title = dataDic["title"] as! String
    var updated_at = dataDic["updated_at"] as! String
    var user_id = dataDic["user_id"] as! Int
    var status = dataDic["status"] as! Int
    var detail = dataDic["details"] as! String
    var donate_date = dataDic["donate_date"] as! String
    
    var post = PostModel(_postOwner:postOwner, _postOwnerFbId: postOwnerFbID, title: title, _details: detail, donateDate: donate_date, id: id, _bloodGroup: bloodGroup, contactDetails: contact_details, createdAt: created_at, flag: flag, hospitalAddress: hospital_address, isDelete: isDelete, patientProblem: patients_problem, postTime: post_time, updateAt: updated_at, userId: user_id)
    
    return post
  }
  
  
  func makePostedUserProfile()->User{
    var user = User()
    user.fbID = "\(seletedPost.postOwnerFbId)"
    user.gender = "loading..."
    user.name = seletedPost.postOwner
    user.hometown = "loading..."
    user.location = "loading..."
    user.email = "loading..."
    user.birthday = "loading"
    user.pictureUrl = BloodBond.sharedInstance.getPicUrlFromFbId(seletedPost.postOwnerFbId)
    return user
    
  }
  
  // MARK: - Navigation
  //  In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  deinit{
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
}


