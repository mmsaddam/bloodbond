//
//  PostView.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/7/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit


let K_PPImageView_tag = 88888
let K_postedUser_tag = 99999
let K_postTitle_tag = 7777
let K_postDetails_tag = 4444

let borderColor = UIColor.lightGrayColor()
//UIColor(red: 145/255, green: 151/255, blue: 163/255, alpha: 1)


class PostView: UIView {
  var ppImageView: UIImageView!
  var indicator: UIActivityIndicatorView!
  var postedUser: UILabel!
  var postTitle: UILabel!
  var isDetails = false
  var delegate: PostViewDelegate!
  
  private var postDetails: UILabel!
  private var bloodGroup: UILabel!
  private var postDate: UILabel!
  private var post = PostModel()
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init(frame: CGRect, post:PostModel ) {
    super.init(frame: frame)
    self.post = post
    self.backgroundColor = Color.milkyWhite
    self.layer.borderColor = borderColor.CGColor
    self.layer.shadowColor = borderColor.CGColor
    self.layer.borderWidth = 1
    self.clipsToBounds = true
    ppImageView = UIImageView(frame: CGRectMake(5, 5, 40, 40))
    ppImageView.image = UIImage(named: "default-fb-pic.jpg")
    ppImageView.layer.cornerRadius = 20
    ppImageView.clipsToBounds = true
    self.addSubview(ppImageView)
    
    indicator = UIActivityIndicatorView()
    indicator.activityIndicatorViewStyle = .Gray
    indicator.center = ppImageView.center
    indicator.startAnimating()
    self.addSubview(indicator)
    
    postedUser = UILabel(frame: CGRectMake(CGRectGetMaxX(ppImageView.frame)+5, 10, CGRectGetWidth(frame)-(CGRectGetMaxX(ppImageView.frame)+2.5), 20))
    postedUser.userInteractionEnabled = true
    postedUser.tag = K_postedUser_tag
    postedUser.textColor = Color.themeColor
    postedUser.font = UIFont(name: fontName, size: FontSize.PostedUserFontSize)
    postedUser.adjustsFontSizeToFitWidth = true
    postedUser.text = post.postOwner + " " + post.bloodGroup
    self.addSubview(postedUser)
    
    postDate = UILabel(frame: CGRectMake(CGRectGetMinX(postedUser.frame), CGRectGetMaxY(postedUser.frame), CGRectGetWidth(postedUser.frame), 15))
    postDate.textColor = Color.postDateColor
    postDate.font = UIFont.systemFontOfSize(FontSize.PostDateFontSize)
    postDate.adjustsFontSizeToFitWidth = true
    
    
    postDate.text = post.post_time
    self.addSubview(postDate)
    
    postTitle = UILabel()
    postTitle.userInteractionEnabled = true
    postTitle.tag = K_postTitle_tag
    postTitle.font = UIFont.systemFontOfSize(FontSize.PostTitleFontSize)
    postTitle.textColor = Color.themeColor
    postTitle.frame = CGRectMake(10, CGRectGetMaxY(ppImageView.frame)+5, CGRectGetWidth(frame)-20, heightForView(post.title, view: postTitle))
    postTitle.numberOfLines = 0
    postTitle.text = post.title
    postTitle.sizeToFit()
    self.addSubview(postTitle)
    
    postDetails = UILabel()
    postDetails.userInteractionEnabled = true
    postDetails.tag = K_postDetails_tag
    postDetails.font = UIFont.systemFontOfSize(FontSize.PostDetailsFontSize)
    postDetails.frame = CGRectMake(10, CGRectGetMaxY(postTitle.frame)+5, CGRectGetWidth(frame)-20, heightForView(post.details, view: postDetails))
    postDetails.text = post.showDetails ? post.details : processPostDetails(post.details)
    postDetails.numberOfLines = 0
    postDetails.sizeToFit()
    postDetails.textColor = Color.customTextColor
    self.addSubview(postDetails)
    var border = UILabel(frame: CGRectMake(0, CGRectGetMaxY(postDetails.frame)+5, CGRectGetWidth(frame), 3))
    border.backgroundColor = borderColor
    self.addSubview(border)
    self.frame = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), CGRectGetMaxY(border.frame))
    
    var str1 = "http://graph.facebook.com/"
    str1  += "\(post.postOwnerFbId)"
    str1 += "/picture?type=large"
    
    self.userInteractionEnabled = true
    
    var tapOnUser = UITapGestureRecognizer(target: self, action: "tapOnUser:")
    postedUser.addGestureRecognizer(tapOnUser)
    
    var tapOnTitle = UITapGestureRecognizer(target: self, action: "tapOnPostTitle:")
    postTitle.addGestureRecognizer(tapOnTitle)
    
    var tapOnDetails = UITapGestureRecognizer(target: self, action: "tapOnPostDetails:")
    postDetails.addGestureRecognizer(tapOnDetails)
    
    NSNotificationCenter.defaultCenter().postNotificationName("ProfileImageDownloadNotifcation", object: self, userInfo: ["imageView":ppImageView,"url":str1,"indicator":indicator])
    ppImageView.addObserver(self, forKeyPath: "image", options:nil, context: nil)
    
    
  }
  
  func heightForView(text:String, view: UILabel) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRectMake(0, 0, view.frame.width, CGFloat.max))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.ByWordWrapping
    label.font = view.font
    label.text = text
    
    label.sizeToFit()
    return label.frame.height
  }
  
  override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
    if keyPath == "image" {
      indicator.stopAnimating()
    }
  }
  
  func processPostDetails(inputStr: String)-> String{
    
    if count(inputStr)<100{
      return inputStr
    }else{
      
      var index = advance(inputStr.startIndex, 90)
      var substring = inputStr.substringToIndex(index)
      return substring + "...Continue Reading"
    }
  }
  
  
  func tapOnUser(gesture: UITapGestureRecognizer){
    
    
    if let delegate = self.delegate{
      delegate.clickOnPostUserName!(makePostedUserProfile())
    
    }
    
    
  }
  
  
  func tapOnPostTitle(gesture: UITapGestureRecognizer){
    
    if let delegate = self.delegate{
      delegate.clickOnPostDetails!(post)
      
    }
  }
  
  
  func tapOnPostDetails(gesture: UITapGestureRecognizer){
    
    if let delegate = self.delegate{
      delegate.clickOnPostDetails!(post)
    }
  }
  
  
  
  func makePostedUserProfile()->User{
    var user = User()
    user.fbID = "\(post.postOwnerFbId)"
    user.gender = "loading..."
    user.name = post.postOwner
    user.hometown = "loading..."
    user.location = "loading..."
    user.email = "loading..."
    user.birthday = "loading"
    user.user_id = String(self.post.user_id)
    user.pictureUrl = BloodBond.sharedInstance.getPicUrlFromFbId(post.postOwnerFbId)
    return user
    
  }
  
  deinit{
    ppImageView.removeObserver(self, forKeyPath: "image")
  }
  
  /*
  // Only override drawRect: if you perform custom drawing.
  // An empty implementation adversely affects performance during animation.
  override func drawRect(rect: CGRect) {
  // Drawing code
  }
  */
  
}
