//
//  MapViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/13/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController,UISearchBarDelegate {

  @IBOutlet var mapView: MKMapView!
  var searchController:UISearchController!
  var annotation:MKAnnotation!
  var localSearchRequest:MKLocalSearchRequest!
  var localSearch:MKLocalSearch!
  var localSearchResponse:MKLocalSearchResponse!
  var error:NSError!
  var pointAnnotation:MKPointAnnotation!
  var pinAnnotationView:MKPinAnnotationView!
  
  let regionRadius: CLLocationDistance = 1000
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.navigationBar.translucent = false
      searchController = UISearchController(searchResultsController: nil)
      searchController.hidesNavigationBarDuringPresentation = false
      self.searchController.searchBar.delegate = self
      presentViewController(searchController, animated: true, completion: nil)
        // Do any additional setup after loading the view.
    }

  
  func searchBarSearchButtonClicked(searchBar: UISearchBar){
    //1
    searchBar.resignFirstResponder()
  //  dismissViewControllerAnimated(true, completion: nil)
    if self.mapView.annotations.count != 0{
      annotation = self.mapView.annotations.first as! MKAnnotation
      self.mapView.removeAnnotation(annotation)
    }
    //2
    localSearchRequest = MKLocalSearchRequest()
    localSearchRequest.naturalLanguageQuery = searchBar.text
    localSearch = MKLocalSearch(request: localSearchRequest)
    localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
      
      if localSearchResponse == nil{
        let alertController = UIAlertController(title: nil, message: "Place Not Found", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        return
      }
      //3
      self.pointAnnotation = MKPointAnnotation()
      self.pointAnnotation.title = searchBar.text
      self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse!.boundingRegion.center.latitude, longitude:     localSearchResponse!.boundingRegion.center.longitude)
      
      
      println("lat \(self.pointAnnotation.coordinate)")
      self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
      self.mapView.centerCoordinate = self.pointAnnotation.coordinate
      self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
    }
  }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
