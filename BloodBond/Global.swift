//
//  Global.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 9/30/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//
import Foundation
import UIKit

let COMMON_URL = "http://dev.bloodbond.co/"
let API_URL = COMMON_URL + "api"
let K_NewPost = "NEW_POST_NOTIFICATION"
let K_UPDATE_PRIVACY = "UPDATE_PRIVACY_SETTINGS"
let K_UPDATE_GENERAL_SETTINGS  = "UPADTE_GENERAL_SETTINGS"
let K_DELETE_POST  = "Delete_My_Post"
let K_UPDATE_POST  = "Update_My_Post"
let K_RE_POST  = "REPOST_My_Post"



struct Color {
  static var themeColor:UIColor = UIColor(red: 0.70588, green: 0.01568, blue: 0.01568, alpha: 1.0)
  static var milkyWhite = UIColor(red: 1, green: 0.988, blue: 1, alpha: 1.0)
  static var textColor:UIColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
  static var navyBlue: UIColor = UIColor(red: 0.236, green: 0.674, blue: 0.874, alpha: 1.0)
  static var customTextColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.9)
  static var postDateColor =  UIColor(red: 145/255, green: 151/255, blue: 163/255, alpha: 1)
}

let dateStyle = NSDateFormatterStyle.MediumStyle

struct FontSize {
  static var PostTitleFontSize: CGFloat = 20
  static var PostedUserFontSize: CGFloat = 17.0
  static var PostDateFontSize: CGFloat = 12
  static var PostDetailsFontSize: CGFloat = 13
  static var InputFieldFontSize: CGFloat = 15
  
}

struct Cordinate {
  var lat: Float = 0.0
  var lon: Float = 0.0
  init(lat: Float, long: Float){
    self.lat = lat
    self.lon = long
  }
}

let fontName = "Helvetica-Bold"

// user blood donation cycle struct
struct Donation {
  var id: Int?
  var donarId: Int?
  var lastDonateDate: String?
  var nextDonateDate: String?
  var totalDonate: Int?
  var daysSinceDonate: Int?
//  init(last: String, next: String, totalDonate: Int, daysSinceDonate:Int){
//    self.lastDonateDate = last
//    self.nextDonateDate = next
//    self.totalDonate = totalDonate
//    self.daysSinceDonate = daysSinceDonate
//  }
}

// user privacy settings
// check user privacy to show info
struct Privacy{
  var cell: Int!
  var dob: Int!
  var email: Int!
  var job: Int!
  var location: Int!
  var IsMail: Int!
  init(cell: Int, dob: Int, email: Int, job: Int, location: Int){
    self.cell = cell
    self.dob = dob
    self.email = email
    self.job = job
    self.location = location
    self.IsMail = 0
  }
}

// structure country name code, full name and phone code
struct CountryCode {
  var nameCode: String!
  var fullName: String!
  var phoneCode: String!
  
  init(nameCode: String, fullName: String, phoneCode: String){
    self.nameCode = nameCode
    self.fullName = fullName
    self.phoneCode = phoneCode
  }
}

struct MapData {
  var name: String!
  var email: String!
  var group: String!
  var curLocCor: Cordinate!
  var homeLocCodinate: Cordinate!
  var cell: String!
  var location: String!
  var homeTown: String!
  var gender: String!
  
}


