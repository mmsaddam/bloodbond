//
//  LabelExtension.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 11/1/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import Foundation

extension UILabel{
  
  func redStar(){
    var main_string = self.text as String!
    var string_to_color = "*"
    var range = (main_string as NSString).rangeOfString(string_to_color)
    var attributedString = NSMutableAttributedString(string:main_string)
    attributedString.addAttribute(NSForegroundColorAttributeName, value: Color.themeColor , range: range)
    self.attributedText = attributedString
  }
}

