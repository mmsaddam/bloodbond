//
//  UserPostsDisplayViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/15/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class UserPostsDisplayViewController: UIViewController,PostViewDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UIAlertViewDelegate {
  
  let edit_tag = 3333
  let delete_tag = 4444
  let repost_tag = 5555
  
  @IBOutlet var tableView: UITableView!
  var allPosts = [PostModel]()
  var selectedPost: PostModel?
  var editActionSheet: UIActionSheet!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.translucent = false
    
    self.title = "My Posts"
    
    self.tableView.dataSource = self
    self.tableView.separatorColor = UIColor.clearColor()
    self.tableView.delegate = self
    self.tableView.reloadData()
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.estimatedRowHeight = 160.0
    
    editActionSheet = UIActionSheet(title: "What do you want?", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", otherButtonTitles: "Edit", "Re-Post")
    
    /**** tap on tableview cell ****/
    var tap = UITapGestureRecognizer(target: self, action: "handleTap:")
    tap.numberOfTapsRequired = 1
    tap.numberOfTouchesRequired = 1
    tap.delegate = self
    tableView.addGestureRecognizer(tap)
    
       NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePost:", name: K_UPDATE_POST, object: nil)
  }
  
  
  override  func  viewDidAppear(animated: Bool) {
    self.navigationController?.navigationBar.translucent = false
  }
  
  func updatePost(notification: NSNotification) {
    
    
    let userInfo = notification.userInfo as! [String: AnyObject]
    //  var updatedPost = userInfo["POST"] as! PostModel?
    
    if let updatedPost = userInfo["POST"] as? PostModel{
      println("delted post \(updatedPost.id)")
      for (index, element) in enumerate(self.allPosts) {
        if element.id == updatedPost.id{
          self.allPosts[index] = updatedPost
          break;
        }
      }
    }
    self.tableView.reloadData()
    
    
    //self.navigationController?.popViewControllerAnimated(true)
  }
  
  
  // MARK: UITableView Datasource
  
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
    if self.allPosts.isEmpty{
      return 20
    }else{
      return UITableViewAutomaticDimension
    }
    
  }
  
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
    if section == 0{
      if self.allPosts.isEmpty{
        let noPostLabel = UILabel(frame: CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 20))
        noPostLabel.textColor = Color.textColor
        noPostLabel.textAlignment = NSTextAlignment.Center
        noPostLabel.text = "No post available"
        return noPostLabel
      }else{
        return nil
      }
      
    }else{
      return nil
    }
    
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.allPosts.count
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let cell: PostCell? = tableView.dequeueReusableCellWithIdentifier("PostCell") as? PostCell
    
    var post = allPosts[indexPath.row]
    cell?.postTitle.text = post.title
    cell?.postDetails.text = post.details.length >= 100 ? post.details[0...99] + "...continue" : post.details
    cell?.postedBy.text = post.postOwner
    cell?.postDate.text = post.post_time
    cell?.bloodGroup.text = post.bloodGroup
    cell?.border.alpha = indexPath.row == 0 ? 0:1
    if post.user_id == BloodBond.sharedInstance.loggedUser.user_id.toInt(){
      cell?.editButton.tag = indexPath.row
      cell?.editButton.addTarget(self, action: "editPost:", forControlEvents: .TouchUpInside)
    }else{
      cell?.editButton.alpha = 0
    }
    
    
    if let img = post.posterImg {
      cell?.PPImageView.image = img
      cell?.indicator.stopAnimating()
    }else{
      dowloadImage(indexPath) { (image) -> Void in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          cell?.PPImageView?.image = image
          post.posterImg = image
          cell?.indicator.stopAnimating()
        })
      }
    }
    
    return cell!
    
  }
  
  func dowloadImage(indexPath: NSIndexPath, completion:(image: UIImage) -> Void){
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
      let link = BloodBond.sharedInstance.getPicUrlFromFbId(self.allPosts[indexPath.row].postOwnerFbId)
      if let url = NSURL(string: link){
        if let data = NSData(contentsOfURL: url){
          if let image = UIImage(data: data){
            completion(image: image)
          }
        }
      }
      
    })
    
  }
  
  func editPost(sender: UIButton){
    self.selectedPost = allPosts[sender.tag]
    editActionSheet.showInView(self.view)
  }
  
  // MARK: UIAction Sheet Delegate
  func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int){
    
    switch buttonIndex{
    case 0:
      
      var alert = UIAlertView()
      alert.tag = delete_tag
      alert.delegate = self
      alert.message = "Do you want to Delete the post?"
      alert.addButtonWithTitle("Yes")
      alert.addButtonWithTitle("No")
      alert.show()
      
    case 2:
      editPost()
      //      var alert = UIAlertView()
      //      alert.tag = edit_tag
      //
      //    alert.delegate = self
      //    alert.message = "Are you sure"
      //    alert.addButtonWithTitle("Ok")
      //    alert.addButtonWithTitle("Cancel")
      //    alert.show()
    case 3:
      
      var alert = UIAlertView()
      alert.tag = repost_tag
      alert.delegate = self
      alert.message = "Do you want to re-post?"
      alert.addButtonWithTitle("Yes")
      alert.addButtonWithTitle("No")
      alert.show()
    default: return
    }
  }
  
  func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
    switch alertView.tag{
    case edit_tag:
      if buttonIndex == 0 {
        editPost()
      }
    case delete_tag:
      if buttonIndex == 0 {
        deletePost()
      }
    case repost_tag:
      if buttonIndex == 0 {
        repost()
      }
      
    default: return
    }
  }
  
  func editPost(){
    if let post = selectedPost{
      let storyBoard = UIStoryboard(name: "Main", bundle: nil)
      let postReqVC = storyBoard.instantiateViewControllerWithIdentifier("MakePostVC") as! MakePostViewController
      postReqVC.editPost = post
      postReqVC.flag = 0
      let nav = UINavigationController(rootViewController: postReqVC)
      self.presentViewController(nav, animated: true, completion: nil)
    }
    
  }
  
  func deletePost(){
    if let post = selectedPost{
      
      let dic = ["action": "postDelete", "post_id" : String(post.id)]
      showHud()
      BloodBond.sharedInstance.postRequest(API_URL, body: dic, completion: { (success, JSON) -> Void in
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          
          self.hideHud()
          
          if success{
            if let status: Int? = JSON["postDelete"] as? Int where status == 1{
              // remove post from local array
              self.removePostElement(post)
              // post delete notiifcation
              NSNotificationCenter.defaultCenter().postNotificationName(K_DELETE_POST, object: self, userInfo: ["POST":post])

              let alert = UIAlertView(title: "Congratulations!", message: "Successfully deleted the post", delegate: nil, cancelButtonTitle: "Ok")
              alert.show()
              
            }else{
              let alert = UIAlertView(title: "Sorry!", message: "Fail to delete", delegate: nil, cancelButtonTitle: "Ok")
              alert.show()
            }
            
            
          }else{
            let alert = UIAlertView(title: "Sorry!", message: "Fail to delete", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            
          }
          
        })
      })
      
    }
    
  }
  
  func repost(){
    if let post = selectedPost{
      var dic = [
        "action" : "rePost",
        "post_id" : String(post.id),
        "user_id" : BloodBond.sharedInstance.loggedUser.user_id
      ]
      self.showHud()
      BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, json) -> Void in
        println("json# \(json)")
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
          self.hideHud()
          if let check = json["rePost"] as? Int{
            if check == 1{
              
             if let data = json["data"] as? [AnyObject]{
                let object: AnyObject? = data.first
              if let post_time: String = object?.valueForKey("post_time") as? String {
                post.post_time = post_time
              }
              if let isDelete: Int = object?.valueForKey("isDelete") as? Int {
                post.isDelete = isDelete
              }
              if let status: Int = object?.valueForKey("status") as? Int {
                post.status = status
              }
              
              self.tableView.reloadData()
              NSNotificationCenter.defaultCenter().postNotificationName(K_RE_POST, object: self, userInfo: ["POST" :post])
                
        }
              
              
              
            }else{
              let alert = UIAlertView(title: "Error!", message: "Fail to re-post", delegate: nil, cancelButtonTitle: "ok")
              alert.show()
            }
          }else{
            let alert = UIAlertView(title: "Error!", message: "Fail to re-post", delegate: nil, cancelButtonTitle: "ok")
            alert.show()
          }
          })
    }
      
    }
  
  }
  
  func removePostElement(post: PostModel){
    
      println("delted post \(post.id)")
    
    for (index, element) in enumerate(self.allPosts) {
      if element.id == post.id{
        self.allPosts.removeAtIndex(index)
        self.tableView.reloadData()
        return
      }
    }
    
  }
  
  // MARK: Handle tap
  
  func handleTap(tap: UITapGestureRecognizer){
    if UIGestureRecognizerState.Ended == tap.state {
      var tableView = tap.view as? UITableView
      let point = tap.locationInView(tap.view)
      let indexPath = tableView?.indexPathForRowAtPoint(point)
      let cell = tableView!.cellForRowAtIndexPath(indexPath!) as? PostCell
      let pointInCell = tap.locationInView(cell)
      if CGRectContainsPoint(cell!.postTitle.frame, pointInCell){
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = self.allPosts[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
      }else if CGRectContainsPoint(cell!.postDetails.frame, pointInCell){
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var singlePostVC = storyboard.instantiateViewControllerWithIdentifier("SinglePostViewController") as! SinglePostViewController
        singlePostVC.post = self.allPosts[indexPath!.row]
        self.navigationController?.pushViewController(singlePostVC, animated: true)
        
        
      }else if CGRectContainsPoint(cell!.postedBy.frame, pointInCell){
        self.navigationController?.popViewControllerAnimated(true)
      }
      
    }
  }
  
  // MARK: UITapGestureRecognoser Delegate
  
  func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool{
    var tableView = gestureRecognizer.view as? UITableView
    let point = gestureRecognizer.locationInView(gestureRecognizer.view)
    if let isFfound = tableView!.indexPathForRowAtPoint(point){
      return true
    }else{
      return false
    }
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  deinit{
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
