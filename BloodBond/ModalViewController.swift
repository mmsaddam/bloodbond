//
//  ModalViewController.swift
//  GoogleMap
//
//  Created by Muzahidul Islam on 10/11/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import AddressBook

class ModalViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate,MKMapViewDelegate {
  enum ActiveAction: Int{
    case BDGroup = 0
    case Country
    case Birthday
  }
  
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var homeLocField: UITextField!
  @IBOutlet var homeLocToolbar: UIToolbar!
  @IBOutlet var curLocField: UITextField!
  @IBOutlet var curentLocationToolbar: UIToolbar!
  // @IBOutlet var map2: MKMapView!
  // @IBOutlet var map1: MKMapView!
  @IBOutlet var birthdayToolbar: UIToolbar!
  @IBOutlet var phoneToolbar: UIToolbar!
  @IBOutlet var firstToolbar: UIToolbar!
  @IBOutlet var emailField: UITextField!
  @IBOutlet var bGSwitch: UISwitch!
  @IBOutlet var curLocSwitch: UISwitch!
  
  @IBOutlet var homeLocSwitch: UISwitch!
  @IBOutlet var bGField: UITextField!
  @IBOutlet var phoneField: UITextField!
  @IBOutlet var countryNameField: UITextField!
  @IBOutlet var countryCodeField: UITextField!
  @IBOutlet var birthdateField: UITextField!
  
  @IBOutlet var phoneSwitch: UISwitch!
  @IBOutlet var birthdateSwitch: UISwitch!
  @IBOutlet var emailSwitch: UISwitch!
  
  var active = ActiveAction(rawValue: 0)
  var itemsBdGroup = ["A-", "A+", "B-", "B+", "AB+", "AB-", "O+", "O-"]
  var itemsCountry = [CountryCode]()
  var tableView: UITableView!
  var blurView: UIView!
  var datePicker: PickerView!
  var manager: CLLocationManager!
  var loggedUser : User!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Modal"
    self.navigationController?.navigationBar.translucent = false

    loggedUser = BloodBond.sharedInstance.loggedUser
    itemsCountry = [
      CountryCode(nameCode: "AF", fullName: "Afghanistan", phoneCode: "+93"),
      CountryCode(nameCode: "AL", fullName: "Albania", phoneCode: "+355"),
      CountryCode(nameCode: "DZ", fullName: "Algeria", phoneCode: "+213"),
      CountryCode(nameCode: "AS", fullName: "American Samoa", phoneCode: "+1"),
      CountryCode(nameCode: "AD", fullName: "Andorra", phoneCode: "+376"),
      CountryCode(nameCode: "AO", fullName: "Angola", phoneCode: "+244"),
      CountryCode(nameCode: "AI", fullName: "Anguilla", phoneCode: "+1"),
      
      CountryCode(nameCode: "AR", fullName: "Antigua", phoneCode: "+1"),
      CountryCode(nameCode: "AR", fullName: "Argentina", phoneCode: "+54"),
      CountryCode(nameCode: "AM", fullName: "Armenia", phoneCode: "+374"),
      CountryCode(nameCode: "AW", fullName: "Aruba", phoneCode: "+297"),
      CountryCode(nameCode: "AU", fullName: "Australia", phoneCode: "+61"),
      CountryCode(nameCode: "AT", fullName: "Austria", phoneCode: "+43"),
      CountryCode(nameCode: "AZ", fullName: "Azerbaijan", phoneCode: "+994")
    ]
    
    
    var state = birthdateSwitch.on ? 1 : 0
    
    self.popoverTableViewSetting()
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(view.frame), CGRectGetHeight(view.frame)+30)
    var tapOnScroll = UITapGestureRecognizer(target: self, action: "handleTapOnScrollView:")
    scrollView.addGestureRecognizer(tapOnScroll)
    
    
    datePicker = PickerView(kframe: CGRectMake(15, CGRectGetHeight(self.view.frame)/2-100, CGRectGetWidth(self.view.frame)-30, 200))
    
    datePicker.done.addTarget(self, action: "datePickerDone:", forControlEvents: UIControlEvents.TouchUpInside)
    datePicker.alpha = 0
    
    self.view.addSubview(datePicker)
    
    manager = CLLocationManager()
    manager.delegate = self
    manager.desiredAccuracy = kCLLocationAccuracyBest
    manager.requestAlwaysAuthorization()
    manager.startUpdatingLocation()
    
    self.setInitalFieldValueFromLoggedUser()
    
    println("switch \(state)")
    
    
    //   centerMapOnLocation(manager.location)
    //  setLocationAnnotationOnMap()
    
    // map1.showsUserLocation = true
    // Do any additional setup after loading the view.
  }
  
  // MARK: setinital field value
  
  func setInitalFieldValueFromLoggedUser(){
    self.emailField.text = loggedUser.email
    self.birthdateField.text = loggedUser.birthday
    self.curLocField.text = loggedUser.location
    self.homeLocField.text = loggedUser.hometown
  }
  
  // MARK: handle tap on scrollview
  
  func handleTapOnScrollView(gesture: UITapGestureRecognizer){
    
    let location = gesture.locationInView(gesture.view)
    
    for index in 0..<scrollView.subviews.count {
      let view = scrollView.subviews[index] as! UIView
      
      if CGRectContainsPoint(view.frame, location){
        if view == firstToolbar{
          let loc = self.firstToolbar.convertPoint(location, fromView: gesture.view)
          if CGRectContainsPoint(bGField.frame, loc){
            self.bGField.resignFirstResponder()
            active = ActiveAction(rawValue: 0)
            self.tableView.reloadData()
            self.tableView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-100,CGRectGetMaxY(firstToolbar.frame), 200, 200)
            tableViewHideAndShow(true)
          }
          
        }else if view == phoneToolbar{
          
          let loc = self.phoneToolbar.convertPoint(location, fromView: gesture.view)
          if CGRectContainsPoint(countryNameField.frame, loc){
            self.phoneField.resignFirstResponder()
            active = ActiveAction(rawValue: 1)
            self.tableView.reloadData()
            self.tableView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-100,CGRectGetMaxY(phoneToolbar.frame), 200, 200)
            tableViewHideAndShow(true)
          }
          
        } else if view == birthdayToolbar{
          let loc = self.birthdayToolbar.convertPoint(location, fromView: gesture.view)
          if CGRectContainsPoint(birthdateField.frame, loc){
            datePicker.showMe()
          }
        }
        
      }
      
      
    }
    
    
  }
  
  
  
  func centerMapOnLocation(location: CLLocation) {
    let regionRadius: CLLocationDistance = 1000
    let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
      regionRadius * 2.0, regionRadius * 2.0)
    // map1.setRegion(coordinateRegion, animated: true)
  }
  
  // MARK: Outlet Action
  
  @IBAction func _phoneSwitch(sender: AnyObject) {
    
  }
  @IBAction func birthdate(sender: AnyObject) {
    
  }
  @IBAction func emailSwitch(sender: AnyObject) {
    
  }
  @IBAction func bdgroupSwitch(sender: AnyObject) {
    
  }
  
  @IBAction func doneAction(sender: AnyObject) {
    
    if fieldValidation(){
      self.userUpdateReq(loggedUser, completion: { (result) -> Void in
        if result == true{
          self.goToHome()
        }
      })
    }
  }
  
  
  func goToHome(){
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      
      let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      appDelegate.createMenuView()
      
    })
    
  }
  
  // date picker done button action
  func datePickerDone(sender: UIButton){
    var formatter = NSDateFormatter()
    formatter.dateFormat = "MM-dd-yyyy"
    formatter.dateStyle = NSDateFormatterStyle.ShortStyle
    
    birthdateField.text = formatter.stringFromDate(datePicker.date())
    datePicker.hideMe()
  }
  
  // MARK: update user
  func userUpdateReq(usr: User, completion:(result: Bool)-> Void){
    
    var dic = [
      "action" : "updateUser",
      "user_id" : loggedUser.user_id,
      "email" : self.emailField.text,
      "dob" : self.birthdateField.text,
      "blood_group" : String(getBloodGroup(bGField.text)),
      "mobile" : self.countryCodeField.text + self.phoneField.text,
      "location" : curLocField.text,
      "cur_lat" : "0.0",
      "cur_long" : "0.0",
      "hometown" : homeLocField.text,
      "home_lat" : "0.0",
      "home_long" : "0.0",
      "cb_email" : "\(emailSwitch.on ? 1:0)",
      "cb_dob" : "\( birthdateSwitch.on ? 1:0)",
      "cb_cell" : "\(phoneSwitch.on ? 1:0)",
      "cb_location1" : "\(curLocSwitch.on ? 1:0)",
      "cb_location2" : "\(homeLocSwitch.on ? 1:0)",
    ]
    //  BloodBond.sharedInstance.printUserDetils(usr)
    
    println("dic\(dic)")
    
    self.showHud()
    
    BloodBond.sharedInstance.postRequest("http://dev.bloodbond.co/api", body: dic) { (success, JSON) -> Void in
      println("update: \(JSON)")
      if let updateUser: String? = JSON.valueForKey("updateUser") as? String{
        if updateUser == "1"{
          
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            BloodBond.sharedInstance.loggedUser.email = self.emailField.text
            BloodBond.sharedInstance.loggedUser.bloodGroup = self.bGField.text
            BloodBond.sharedInstance.loggedUser.mobile = self.phoneField.text
            BloodBond.sharedInstance.loggedUser.location = self.curLocField.text
            BloodBond.sharedInstance.loggedUser.hometown = self.homeLocField.text
            self.hideHud()
            completion(result: true)
          })
          
        }else{
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.hideHud()
            println("fail to update user")
            var alert = UIAlertView(title: "Error!!", message: "You are not matured enought", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            completion(result: false)
          })
          
        }
        
      }
      
    }
    
  }
  
  
  func getBloodGroup(tag: String)-> Int{
    switch tag {
    case "A-":return 1
    case "A+": return 2
    case "B-":return 3
    case "B+": return 4
    case "O-":return 5
    case "O+": return 6
    case "AB-": return 7
    default: return 8
    }
  }
  
  // MARK: field validation
  func fieldValidation()-> Bool{
    let alert = UIAlertView()
    alert.addButtonWithTitle("Ok")
    alert.title = "Error!!"
    
    if bGField.text.isEmpty{
      alert.message = "Please enter your blood group"
      alert.show()
      return false
    }else if emailField.text.isEmpty{
      alert.message = "Please enter your email"
      alert.show()
      return false
    }else if !emailValidation(emailField.text){
      alert.message = "Please enter valid email"
      alert.show()
      return false
    }else if birthdateField.text.isEmpty{
      alert.message = "Please enter birthday"
      alert.show()
      return false
    }else if phoneField.text.isEmpty{
      alert.message = "Please enter your phone"
      alert.show()
      return false
    }else if curLocField.text.isEmpty{
      alert.message = "Please enter current location"
      alert.show()
      return false
    }else if homeLocField.text.isEmpty{
      alert.message = "Please enter home location"
      alert.show()
      return false
    }else{
      return true
    }
  }
  
  
  func emailValidation(email: String)-> Bool{
    let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
    return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(email)
  }
  
  // MARK:- UITableView Datasource
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    if active?.rawValue == 0{
      return itemsBdGroup.count
    }else{
      return itemsCountry.count
    }
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    
    var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
    if active?.rawValue == 0{
      cell.textLabel?.text = itemsBdGroup[indexPath.row]
    }else{
      var country = itemsCountry[indexPath.row]
      cell.textLabel?.text = country.fullName
    }
    
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if active?.rawValue == 0{
      self.bGField.text = itemsBdGroup[indexPath.row]
      bGField.resignFirstResponder()
      tableViewHideAndShow(false)
    }else{
      var country = itemsCountry[indexPath.row]
      countryNameField.text = country.nameCode
      countryCodeField.text = country.phoneCode
      tableViewHideAndShow(false)
    }
    
  }
  
  // MARK: tableView control
  
  
  func popoverTableViewSetting(){
    tableView = UITableView(frame: CGRectMake(0, 10, 200, 200), style: UITableViewStyle.Plain)
    
    self.view.addSubview(tableView)
    tableView.dataSource = self
    tableView.delegate = self
    tableView.alpha = 0;
    self.view.bringSubviewToFront(self.tableView)
    
    
    tableView.layer.borderWidth = 1
    tableView.layer.borderColor = UIColor.grayColor().CGColor
    tableView.layer.masksToBounds = true
  }
  
  func tableViewHideAndShow(isVisible:Bool){
    if isVisible {
      self.view.bringSubviewToFront(tableView)
      self.view.endEditing(true)
    }
    removeBlur()
    isVisible ?applyBlurEffect():removeBlur()
    [UIView .animateWithDuration(0.3, animations: { () -> Void in
      self.tableView.alpha = isVisible ? 1:0
      
      }, completion:nil
      )];
    
  }
  
  // MARK: blur effect
  
  func applyBlurEffect(){
    blurView = UIView().makeViewBLur(self.view.bounds)
    view.insertSubview(blurView, belowSubview: tableView)
    
  }
  
  func removeBlur(){
    if (blurView != nil) {
      blurView.removeFromSuperview()
      blurView = nil
    }
  }
  
  // MARK: UITextField Delegate
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
    if textField == self.bGField{
      
    }
    return true
  }
  func textFieldDidBeginEditing(textField: UITextField){
    
  }
  func textFieldShouldEndEditing(textField: UITextField) -> Bool{
    return true
  }
  func textFieldShouldReturn(textField: UITextField) -> Bool{
    textField.resignFirstResponder()
    return true
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
