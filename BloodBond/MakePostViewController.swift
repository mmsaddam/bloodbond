//
//  MakePostViewController.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/17/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class MakePostViewController: UIViewController,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
  
  enum Action{
    case NewPost, EditPost
  }
  
   let dimention: CGFloat = 75.0
   var scrollView: UIScrollView!
   var doneButton: UIButton!
   var detailsTextView: UITextView!
   var detailsLabel: UILabel!
   var patientsPLabel: UILabel!
   var dateField: UITextField!
   var dateLabel: UILabel!
   var bloodGrpField: UITextField!
   var bloodGrpLabel: UILabel!
   var hospitalField: UITextField!
   var hospitalLabel: UILabel!
   var contactField: UITextField!
   var contactLabel: UILabel!
   var titleField: UITextField!
   var titleLabel: UILabel!
   var patientProblem: UITextView!
  
  var editPost: PostModel?
  var action: Action?
  var flag = 0
  
 private var PPP: UILabel!
 private var PPDP: UILabel!
 private var tableView: UITableView!
 private var blurView: UIView!
 private var datePicker: PickerView!
 private var itemsBdGroup = ["A-", "A+", "B-", "B+", "AB+", "AB-", "O+", "O-"]
 private var loggedUser = BloodBond.sharedInstance.loggedUser
  
  private var leftItem: UIBarButtonItem!
  private var rightItem: UIBarButtonItem!
  
  override func viewDidLoad() {
    super.viewDidLoad()
   
    self.navigationController?.navigationBar.translucent = false
    self.automaticallyAdjustsScrollViewInsets = false
    self.popoverTableViewSetting()
    
    datePicker = PickerView(kframe: CGRectMake(15, CGRectGetHeight(self.view.frame)/2-100, CGRectGetWidth(self.view.frame)-30, 200))
    datePicker.done.addTarget(self, action: "datePickerDone:", forControlEvents: UIControlEvents.TouchUpInside)
    datePicker.alpha = 0
    self.view.addSubview(datePicker)
    
    UIFixing()
    
    
    
    if flag == 0{
      action = Action.EditPost
    }else{
      action = Action.NewPost
    }
    leftItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "cancel:")
    self.navigationItem.leftBarButtonItem = leftItem
    
    
    if let acc = self.action{
      switch acc{
      case .EditPost:
         self.title = "Edit Post"
       rightItem = UIBarButtonItem(title: "Update", style: .Plain, target: self, action: "update:")
       self.navigationItem.rightBarButtonItem = rightItem
        setEditPostField()
      case .NewPost:
         self.title = "New Post"
        rightItem = UIBarButtonItem(title: "Save", style: .Plain, target: self, action: "newPost:")
        self.navigationItem.rightBarButtonItem = rightItem
      }
    }
    
    var tapOnScroll = UITapGestureRecognizer(target: self, action: "handleTapOnScrollView:")
    scrollView.addGestureRecognizer(tapOnScroll)
    
  }
 
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    
  }
  
   func newPost(sender: AnyObject) {
    if postFromValidation(){
     self.sendPostReq()
    }
  }

  func update(sender: AnyObject){
    if postFromValidation(){
      if let post = editPost{
           self.sendPostUpdateReq(post)
      }
    
    }
  }
  
  func cancel(sender: AnyObject){
    self.dismissViewControllerAnimated(true, completion: nil)
  }
  
  func setEditPostField(){
    if let post = editPost{
      self.titleField.text = post.title
      self.detailsTextView.text = post.details
      self.contactField.text = post.contact_details
      self.hospitalField.text = post.hospital_address
      self.bloodGrpField.text = post.bloodGroup
    //  self.dateField.text = post.donate_date
      self.patientProblem.text = post.patients_problem
      PPP.alpha = 0
      PPDP.alpha = 0
    }
  }
  
  // MARK: UIFixing
  func UIFixing(){
    
    scrollView = UIScrollView(frame: UIScreen.mainScreen().bounds)
    
    let titleLabel = UILabel(frame: CGRectMake(10, 10, CGRectGetWidth(scrollView.frame)-20, 20))
    titleLabel.attributedText = "Title"["*"]
    scrollView.addSubview(titleLabel)
    
    titleField = UITextField(frame: CGRectMake(15, CGRectGetMaxY(titleLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 30))
    titleField.delegate = self
    titleField.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)
    titleField.borderStyle = UITextBorderStyle.RoundedRect
    titleField.autocapitalizationType = .None
    titleField.placeholder = "Post title"
    titleField.layer.borderColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
    scrollView.addSubview(titleField)
    
    
    let contactLabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(titleField.frame)+7, CGRectGetWidth(scrollView.frame)-20, 20))
    contactLabel.attributedText = "Contact Details"["*"]
    scrollView.addSubview(contactLabel)
    
    contactField = UITextField(frame: CGRectMake(15, CGRectGetMaxY(contactLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 30))
    contactField.delegate = self
    contactField.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)
    contactField.borderStyle = UITextBorderStyle.RoundedRect

    contactField.autocapitalizationType = .None
    contactField.placeholder = "Contact details"
    contactField.layer.borderColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
    scrollView.addSubview(contactField)
    
    
    let hospitalLabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(contactField.frame)+7, CGRectGetWidth(scrollView.frame)-20, 20))
    hospitalLabel.attributedText = "Hospital Address"["*"]
    scrollView.addSubview(hospitalLabel)
    
    hospitalField = UITextField(frame: CGRectMake(15, CGRectGetMaxY(hospitalLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 30))
    hospitalField.borderStyle = UITextBorderStyle.RoundedRect
    hospitalField.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)

    hospitalField.autocapitalizationType = .None
    hospitalField.delegate = self
    hospitalField.placeholder = "Hospital address"
    hospitalField.layer.borderColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).CGColor
    scrollView.addSubview(hospitalField)
    
    let bloodLabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(hospitalField.frame)+7, CGRectGetWidth(scrollView.frame)-20, 20))
    bloodLabel.attributedText = "Required Blood Group"["*"]
    scrollView.addSubview(bloodLabel)
    
    bloodGrpField = UITextField(frame: CGRectMake(15, CGRectGetMaxY(bloodLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 30))
    bloodGrpField.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)

    bloodGrpField.addArrow()
    bloodGrpField.borderStyle = UITextBorderStyle.RoundedRect

    bloodGrpField.autocapitalizationType = .None
    bloodGrpField.delegate = self
    bloodGrpField.placeholder = "Blood Group"
    bloodGrpField.text = "A-"
    bloodGrpField.layer.borderColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
    bloodGrpField.userInteractionEnabled = false
    scrollView.addSubview(bloodGrpField)
    
    let dateLabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(bloodGrpField.frame)+7, CGRectGetWidth(scrollView.frame)-20, 20))
    dateLabel.attributedText = "Required Date"["*"]
    scrollView.addSubview(dateLabel)
    
    dateField = UITextField(frame: CGRectMake(15, CGRectGetMaxY(dateLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 30))
    dateField.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)

    dateField.addArrow()
    dateField.borderStyle = UITextBorderStyle.RoundedRect
    dateField.delegate = self
    dateField.placeholder = "Required date"
    dateField.layer.borderColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
    dateField.userInteractionEnabled = false
    scrollView.addSubview(dateField)
    
    let patientProbLabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(dateField.frame)+7, CGRectGetWidth(scrollView.frame)-20, 20))
    patientProbLabel.attributedText = "Patient't Problem"["*"]
    scrollView.addSubview(patientProbLabel)
    
    patientProblem = UITextView(frame: CGRectMake(15, CGRectGetMaxY(patientProbLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 110))
    patientProblem.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)

    patientProblem.delegate = self
    patientProblem.autocapitalizationType = .None
    patientProblem.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
    patientProblem.layer.borderWidth = 1.0
    patientProblem.layer.cornerRadius = 4
    patientProblem.tag = 1
    scrollView.addSubview(patientProblem)
    
    PPP = UILabel(frame: CGRectMake(5.0, 5.0, CGRectGetWidth(patientProblem.frame) - 5.0, 20.0))
    PPP.font = patientProblem.font
    PPP.textColor = UIColor.lightGrayColor()
    PPP.text = "Patient's Problem"
    patientProblem.addSubview(PPP)
    patientProblem.delegate = self
   
    
    let problemDetaiLabel = UILabel(frame: CGRectMake(10, CGRectGetMaxY(patientProblem.frame)+7, CGRectGetWidth(scrollView.frame)-20, 20))
    problemDetaiLabel.attributedText = "Details"["*"]
    scrollView.addSubview(problemDetaiLabel)
    
    detailsTextView = UITextView(frame: CGRectMake(15, CGRectGetMaxY(problemDetaiLabel.frame)+5, CGRectGetWidth(scrollView.frame)-30, 150))
    detailsTextView.delegate = self
    detailsTextView.font = UIFont.systemFontOfSize(FontSize.InputFieldFontSize)

    detailsTextView.autocapitalizationType = .None
    detailsTextView.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).CGColor
    detailsTextView.layer.borderWidth = 1.0
    detailsTextView.layer.cornerRadius = 4
    detailsTextView.tag = 1
    scrollView.addSubview(detailsTextView)
   
    
    PPDP = UILabel(frame: CGRectMake(5.0, 5.0, CGRectGetWidth(patientProblem.frame) - 5.0, 20.0))
    PPDP.textColor = UIColor.lightGrayColor()
    PPDP.font = detailsTextView.font
    PPDP.text = "Details..."
    detailsTextView.addSubview(PPDP)
    detailsTextView.delegate = self
    
    scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame), CGRectGetMaxY(detailsTextView.frame)+350)
    
    self.view.addSubview(scrollView)
    
  }
  
  
  // MARK: handle tap on scrollview
  func handleTapOnScrollView(gesture: UITapGestureRecognizer){
  
    let location = gesture.locationInView(gesture.view)
    
    for index in 0..<scrollView.subviews.count {
      let view = scrollView.subviews[index] as! UIView
      
      if CGRectContainsPoint(view.frame, location){
        if view == bloodGrpField{
          self.tableView.reloadData()
          self.tableView.frame = CGRectMake(CGRectGetWidth(self.view.frame)/2-100,CGRectGetMaxY(bloodGrpField.frame), 200, 200)
          tableViewHideAndShow(true)
        }else if view == dateField{
           self.view.endEditing(true)
          self.view.bringSubviewToFront(datePicker)
          datePicker.showMe()
        }
        
      }
    }
    
    
  }
  
  
  // date picker done button action
  func datePickerDone(sender: UIButton){
    var formatter = NSDateFormatter()
    formatter.dateFormat = "MM-dd-yyyy"
    formatter.dateStyle = NSDateFormatterStyle.ShortStyle
    
    dateField.text = formatter.stringFromDate(datePicker.date())
    datePicker.hideMe()
  }
  
  
  // MARK: tableView control
  func popoverTableViewSetting(){
    tableView = UITableView(frame: CGRectMake(0, 10, 200, 200), style: UITableViewStyle.Plain)
    
    self.view.addSubview(tableView)
    tableView.dataSource = self
    tableView.delegate = self
    tableView.alpha = 0;
    self.view.bringSubviewToFront(self.tableView)

    tableView.layer.borderWidth = 1
    tableView.layer.borderColor = UIColor.grayColor().CGColor
    tableView.layer.masksToBounds = true
  }
  
  func tableViewHideAndShow(isVisible:Bool){
    if isVisible {
      self.view.bringSubviewToFront(tableView)
      self.view.endEditing(true)
    }
    removeBlur()
    isVisible ?applyBlurEffect():removeBlur()
    [UIView .animateWithDuration(0.3, animations: { () -> Void in
      self.tableView.alpha = isVisible ? 1:0
      
      }, completion:nil
      )];
    
  }
  
  
  // MARK: Post new status
  @IBAction func doneAction(sender: AnyObject) {
    if postFromValidation(){
      self.sendPostReq()
      
    }
  }
  
  // MARK:  send post request
  func sendPostReq(){
    var dic = [
      "action" : "savePost",
      "user_id" : loggedUser.user_id,
      "post_title" : titleField.text,
      "post_details" : detailsTextView.text,
      "contact_details" : contactField.text,
      "donation_date" : dateField.text,
      "patients_problem" : patientProblem.text,
      "hospital_address" : hospitalField.text,
      "blood_group" : String(getBloodGroup(bloodGrpField.text))
    ]
    self.showHud()
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, json) -> Void in
      println("json# \(json)")
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        if success{
          
          if let check = json["savePost"] as? Int{
            if check == 1{
              NSNotificationCenter.defaultCenter().postNotificationName(K_NewPost, object: self, userInfo: nil)
              self.dismissViewControllerAnimated(true, completion: nil)

              
            }else{
              let alert = UIAlertView(title: "Error!", message: "Fail to post", delegate: nil, cancelButtonTitle: "ok")
              
              alert.show()
              
            }
          }
          
          
        }else{
          let alert = UIAlertView(title: "Error!", message: "Fail to post", delegate: nil, cancelButtonTitle: "ok")
          alert.show()
        }
        
      })
    }
  }
  
  func sendPostUpdateReq(post: PostModel){
    

    var dic = [
      "action" : "updatePost",
      "post_id" : String(post.id),
      "post_title" : titleField.text,
      "post_details" : detailsTextView.text,
      "contact_details" : contactField.text,
      "donation_date" : dateField.text,
      "patients_problem" : patientProblem.text,
      "hospital_address" : hospitalField.text,
      "blood_group" : String(getBloodGroup(bloodGrpField.text))
    ]
    self.showHud()
    BloodBond.sharedInstance.postRequest(API_URL, body: dic) { (success, json) -> Void in
      println("json# \(json)")
      
      dispatch_async(dispatch_get_main_queue(), { () -> Void in
        if success{
          self.hideHud()
          if let check = json["updatePost"] as? Int{
            if check == 1{
              post.title = self.titleField.text
              post.details = self.detailsTextView.text
              post.contact_details = self.contactField.text
              post.patients_problem = self.patientProblem.text
              post.hospital_address = self.hospitalField.text
              post.bloodGroup = self.bloodGrpField.text

                if let data = json["data"] as? [AnyObject]{
                  let object: AnyObject? = data.first
                  if let post_time: String = object?.valueForKey("post_time") as? String {
                    post.post_time = post_time
                  }
                  
                  if let donate_time: String = object?.valueForKey("donate_date") as? String {
                    post.donate_date = donate_time
                  }
                }
                
                NSNotificationCenter.defaultCenter().postNotificationName(K_UPDATE_POST, object: self, userInfo: ["POST" :post])
                self.dismissViewControllerAnimated(true, completion: nil)
                                
              
              
            }else{
              let alert = UIAlertView(title: "Error!", message: "Fail to post", delegate: nil, cancelButtonTitle: "ok")
              
              alert.show()
              
            }
          }
          
          
        }else{
          let alert = UIAlertView(title: "Error!", message: "Fail to post", delegate: nil, cancelButtonTitle: "ok")
          alert.show()
        }
        
      })
    }
  }

  
  func getBloodGroup(tag: String)-> Int{
    switch tag {
    case "A-":return 1
    case "A+": return 2
    case "B-":return 3
    case "B+": return 4
    case "O-":return 5
    case "O+": return 6
    case "AB-": return 7
    default: return 8
    }
  }
  
  
  // MARK: field validation
  func postFromValidation()-> Bool{
    let alert = UIAlertView()
    alert.title = "Error!"
    alert.addButtonWithTitle("Ok")
    if titleField.text.isEmpty{
      alert.message = "Please enter post title"
      alert.show()
      return false
    }else if contactField.text.isEmpty{
      alert.message = "Please enter contact details"
      alert.show()
      return false
    }else if hospitalField.text.isEmpty{
      alert.message = "Please enter hospital address"
      alert.show()
      return false
    }else if bloodGrpField.text.isEmpty {
      alert.message = "Please enter required blood group"
      alert.show()
      return false
    }else if dateField.text.isEmpty{
      alert.message = "Please enter required date"
      alert.show()
      return false
    }else if patientProblem.text.isEmpty{
      alert.message = "Please enter patient's problem"
      alert.show()
      return false
    }else if detailsTextView.text.isEmpty{
      alert.message = "Please enter patient's problem details"
      alert.show()
      return false
    }else {
      return true
    }
  }
  
  
  // MARK:- UITableView Datasource
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    return itemsBdGroup.count
    
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    
    var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
    cell.textLabel?.text = itemsBdGroup[indexPath.row]
    
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    self.bloodGrpField.text = itemsBdGroup[indexPath.row]
    tableViewHideAndShow(false)
    
  }
  
  // MARK: blur effect
  func applyBlurEffect(){
    blurView = UIView().makeViewBLur(self.view.bounds)
    view.insertSubview(blurView, belowSubview: tableView)
    
  }
  
  func removeBlur(){
    if (blurView != nil) {
      blurView.removeFromSuperview()
      blurView = nil
    }
  }
  
  // MARK: uitexfield delegate
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  
  // MARK: UITextView Delegate
//  func textViewShouldBeginEditing(textView: UITextView) -> Bool {
//    self.patientProblem.text = ""
//    self.patientProblem.textColor = UIColor.blackColor()
//
//     if textView == self.patientProblem{
//        }
//    
//    if textView == self.detailsTextView{
//      self.detailsTextView.text = ""
//      self.detailsTextView.textColor = UIColor.blackColor()
//    }
//    
//    return true
//  }
  
  func textViewDidEndEditing(textView: UITextView) {
  
      if patientProblem.text.isEmpty{
         PPP.alpha = 1
      }
    if detailsTextView.text.isEmpty{
      PPDP.alpha = 1
    }

  }
  
  func textViewDidChange(textView: UITextView) {
    
      if patientProblem.text.isEmpty{
        PPP.alpha = 1
      }else{
         PPP.alpha = 0
      }
    
    if detailsTextView.text.isEmpty{
      PPDP.alpha = 1
    }else{
      PPDP.alpha = 0
    }
    
  }
  
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
