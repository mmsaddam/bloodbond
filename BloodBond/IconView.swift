//
//  IconView.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/3/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class IconView: UIView {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    init(frame:CGRect, image: UIImage, title: String, text: String){
        super.init(frame: frame)
        var imageView = UIImageView(frame: CGRectMake(CGRectGetMidX(frame)-30, 5, 60, 60))
        imageView.image = image
        self.addSubview(imageView)
        
        var titleLabel = UILabel(frame: CGRectMake(CGRectGetMinX(frame), CGRectGetMaxY(imageView.frame), CGRectGetWidth(frame), 30))
        titleLabel.text = title
      
        titleLabel.textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        titleLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(titleLabel)
        
        var desLabel = UILabel(frame: CGRectMake(5, CGRectGetMaxY(titleLabel.frame), CGRectGetWidth(frame)-10, CGRectGetHeight(frame)-(CGRectGetMaxY(titleLabel.frame)+5)))
        desLabel.numberOfLines = 0
        desLabel.adjustsFontSizeToFitWidth = true
       // desLabel.sizeToFit()
        desLabel.font = UIFont.systemFontOfSize(14)
        desLabel.textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        desLabel.text = text
        desLabel.textAlignment = NSTextAlignment.Left
        self.addSubview(desLabel)
        
        self.layer.borderWidth = 2
        self.layer.cornerRadius = 4
        self.layer.borderColor = Color.themeColor.CGColor
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
