//
//  PostTableViewCell.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/6/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var postDate: UILabel!
    @IBOutlet var postDetails: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var pp: UIImageView!
    class var identifier: String {
        return String.className(self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup()

    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
     func setup() {
//        postDetails.numberOfLines = 0
//        var frame = pp.frame
//        pp.layer.cornerRadius = frame.size.width/2
//        pp.clipsToBounds = true
//    
//        
//        postDetails.adjustsFontSizeToFitWidth = true
//        postDetails.numberOfLines = 0
//        postDetails.sizeToFit()
//    
//        userName.adjustsFontSizeToFitWidth = true
//        postDate.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
