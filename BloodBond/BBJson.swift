//
//  BBJson.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/15/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class BBJson: NSObject {
  override init() {
    super.init()
  }
  
  
  // face user data from  return by facebook
  
  func parseFacebookData(json:AnyObject)->User{
    var keys = json.allKeys
    
    //  println("fetched user: \(result)")
    
    var allkeys = json.allKeys
    var loggedUser = User()
    
    for key in allkeys{
      switch key as! String{
        
      case "id":
        loggedUser.fbID = json.valueForKey(key as! String) as! String
        
      case "name":
        loggedUser.name = json.valueForKey(key as! String) as! String
        
      case "gender":
        loggedUser.gender = json.valueForKey(key as! String) as! String
        
      case "hometown":
        var data: AnyObject = json.valueForKey(key as! String)!
        loggedUser.hometown = data.valueForKey("name") as! String
        
      case "location":
        var data: AnyObject = json.valueForKey(key as! String)!
        loggedUser.location = data.valueForKey("name") as! String
        
      case "email":
        loggedUser.email = json.valueForKey(key as! String) as! String
        
      case "birthday":
        loggedUser.birthday = json.valueForKey(key as! String) as! String
        
        
      case "picture" :
        var picture: AnyObject = json.valueForKey(key as! String)!
        var data: AnyObject = picture.valueForKey("data")!
        var picUrlStr = data.valueForKey("url") as! String
        loggedUser.pictureUrl = picUrlStr
        
      case "work" :
        var dataArray: [AnyObject] = json.valueForKey(key as! String)! as! [AnyObject]
        var data: AnyObject = dataArray[0]
        var rootObj: AnyObject? = data.valueForKey("employer")
        loggedUser.work = rootObj!.valueForKey("name") as! String
        
        
      default:
        println("default")
      }
    }
    
    return loggedUser
  }

  
  // MARK: fetch personal Details
  
  func fetchUserDetails(json: AnyObject)->User{
    
    var user = User();
    if let data: AnyObject = json["data"]{
     
      user.user_id = String(data.valueForKey("id") as! Int)
      user.email = data.valueForKey("email") as! String
      user.name = data.valueForKey("name") as! String
      user.mobile = data.valueForKey("cell") as! String
      user.birthday = data.valueForKey("dob") as! String
      user.gender = data.valueForKey("gender") as! String
      user.location = data.valueForKey("location") as! String
      user.mobile = data.valueForKey("cell") as! String
      user.work = data.valueForKey("job") as! String
      user.bloodGroup = data.valueForKey("group") as! String
      
      var fbId: AnyObject? = data.valueForKey("fbid")
      user.fbID = "\(fbId)".removeOptional()
      user.pictureUrl = BloodBond.sharedInstance.getPicUrlFromFbId(fbId!)
      
      user.myPosts = self.fetchMyPost(json)
      user.bloodReq = self.fetchBloodReq(json)
      user.donation = self.fetchDonationInfo(json)
      user.privacy = self.fetchUserPrivacy(json)
      user.mapDataAry = self.fetchMapData(json)
  
     
    }else{
       println("user data not found")
    }
    
    return user
  }


  
  // MARK: fetche all posts of the uesr
  func fetchMyPost(json: AnyObject)-> [PostModel]{
    var tmpAry = [PostModel]()
    if let postsAry: [AnyObject] = json["myposts"] as? Array{
      for dataDic in postsAry{
        
        var postOwner = dataDic["name"] as! String
        var postOwnerFbID: AnyObject = dataDic["fbid"]!!
        var bloodGroup  = dataDic["blood_group"] as! Int
        var contact_details = dataDic["contact_details"] as! String
        var created_at = dataDic["created_at"] as! String
        var flag = dataDic["flag"] as! Int
        var hospital_address = dataDic["hospital_address"] as! String
        var id = dataDic["id"] as! Int
        var isDelete = dataDic["isDelete"] as! Int
        var patients_problem = dataDic["patients_problem"] as! String
        var post_time = dataDic["post_time"] as! String
        var title = dataDic["title"] as! String
        var updated_at = dataDic["updated_at"] as! String
        var user_id = dataDic["user_id"] as! Int
        var status = dataDic["status"] as! Int
        var detail = dataDic["details"] as! String
        var donate_date = dataDic["donate_date"] as! String
        
        
        var post = PostModel(_postOwner:postOwner, _postOwnerFbId: postOwnerFbID, title: title, _details: detail, donateDate: donate_date, id: id, _bloodGroup: bloodGroup, contactDetails: contact_details, createdAt: created_at, flag: flag, hospitalAddress: hospital_address, isDelete: isDelete, patientProblem: patients_problem, postTime: post_time, updateAt: updated_at, userId: user_id)
        
        tmpAry.append(post)
      }
    }
    return tmpAry
  }
  
  // MARK: fetche all posts of the uesr
  func fetchBloodReq(json: AnyObject)-> [PostModel]{
    var tmpAry = [PostModel]()
    if let postsAry: [AnyObject] = json["posts"] as? Array{
      for dataDic in postsAry{
        
        var postOwner = dataDic["name"] as! String
        var postOwnerFbID: AnyObject = dataDic["fbid"]!!
        var bloodGroup  = dataDic["blood_group"] as! Int
        var contact_details = dataDic["contact_details"] as! String
        var created_at = dataDic["created_at"] as! String
        var flag = dataDic["flag"] as! Int
        var hospital_address = dataDic["hospital_address"] as! String
        var id = dataDic["id"] as! Int
        var isDelete = dataDic["isDelete"] as! Int
        var patients_problem = dataDic["patients_problem"] as! String
        var post_time = dataDic["post_time"] as! String
        var title = dataDic["title"] as! String
        var updated_at = dataDic["updated_at"] as! String
        var user_id = dataDic["user_id"] as! Int
        var status = dataDic["status"] as! Int
        var detail = dataDic["details"] as! String
        var donate_date = dataDic["donate_date"] as! String
        
        
        var post = PostModel(_postOwner:postOwner, _postOwnerFbId: postOwnerFbID, title: title, _details: detail, donateDate: donate_date, id: id, _bloodGroup: bloodGroup, contactDetails: contact_details, createdAt: created_at, flag: flag, hospitalAddress: hospital_address, isDelete: isDelete, patientProblem: patients_problem, postTime: post_time, updateAt: updated_at, userId: user_id)
        
        tmpAry.append(post)
      }
    }
    return tmpAry
  }

  
  // MARK: fetch user map data
  func fetchMapData(json: AnyObject)-> [MapData]{
    
    var tempAry = [MapData]()
    if let mapDataAry: [AnyObject] = json["map_data"] as? Array{
      for object in mapDataAry{
        var name = object.valueForKey("name") as! String
        var cell = object.valueForKey("cell") as! String
        var email = object.valueForKey("email") as! String
        var location = object.valueForKey("location") as! String
        var hometown = object.valueForKey("location2") as! String
        var lat1 = object.valueForKey("lat") as! Float
        var long1 = object.valueForKey("long") as! Float
        var lat2 = object.valueForKey("lat2") as! Float
        var long2 = object.valueForKey("long2") as! Float
        var curCor = Cordinate(lat:lat1, long:long1)
        var homeCor = Cordinate(lat:lat2, long:long2)
        
        var mapData = MapData()
        mapData.name = name
        mapData.email = email
        mapData.cell = cell
        mapData.homeTown = hometown
        mapData.location = location
        mapData.curLocCor = curCor
        mapData.homeLocCodinate = homeCor
        tempAry.append(mapData)
        
      }
    }
    return tempAry
  }
  
  
  // MARK: fetch User blood donation info
  func fetchDonationInfo(json: AnyObject)->Donation{
     var donation = Donation()
   
    if let data: AnyObject = json["donate"]{
    
      if let id = data["id"] as? Int{
        donation.id = id
      }
      
      if let donarId = data["user_id"] as? Int{
        donation.donarId = donarId
      }
      
      if let totalDonate = data["totalCount"] as? Int{
        donation.totalDonate = totalDonate
      }
      
      if let daysSince = data["totalDays"] as? Int{
        donation.daysSinceDonate = daysSince
      }
      
      if let lastD = data["donate_date"] as? String{
        donation.lastDonateDate = lastD.toDate
      }
      
      if let nextD = data["nextDonate"] as? String{
        donation.nextDonateDate = nextD.toDate
      }
      
    }
    return donation
  }
  
  // MARK: fetch user privacy settings data
  func fetchUserPrivacy(json: AnyObject)->Privacy{
    var privacy: Privacy!
    
    if let data: AnyObject = json["privacy_settings"]{
      var cell  = data.valueForKey("cell") as! Int
      var dob  = data.valueForKey("dob") as! Int
      var email = data.valueForKey("email") as! Int
      var location  = data.valueForKey("location") as! Int
      var job = data.valueForKey("job") as! Int
      privacy = Privacy(cell: cell, dob: dob, email: email, job: job, location: location)
      
    }else{
      println("cann't face privacy settings")
    }
    
    return privacy
  }

}
