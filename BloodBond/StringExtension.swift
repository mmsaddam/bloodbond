//
//  StringExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

extension String {
    static func className(aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).componentsSeparatedByString(".").last!
    }
  var length: Int {
    return count(self)
  }
  
  var Tag: Int{
    switch self {
    case "A-":return 1
    case "A+": return 2
    case "B-":return 3
    case "B+": return 4
    case "O-":return 5
    case "O+": return 6
    case "AB-": return 7
    default: return 8
    }
  }
  
  var toDate: String{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd h:mm:ss"
    var date = dateFormatter.dateFromString(self)
    dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
    let str = dateFormatter.stringFromDate(date!)
    return str
  }
  
  var floatValue: Float {
    return (self as NSString).floatValue
  }
  
  func removeOptional()->String{
    var str: String = self.stringByReplacingOccurrencesOfString("\\Optional[(](\\w+)[)]", withString: "$1", options: NSStringCompareOptions.RegularExpressionSearch, range: Range<String.Index> (start: self.startIndex, end: self.endIndex))
    return str
  }
  
    subscript (i: Int) -> Character {
        return self[advance(self.startIndex, i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
  
    subscript (r: Range<Int>) -> String {
        return substringWithRange(Range(start: advance(startIndex, r.startIndex), end: advance(startIndex, r.endIndex)))
    }
  
  subscript(colorStr: String) -> NSMutableAttributedString{
    
      var main_string = self + "*"
      var string_to_color = colorStr
      var range = (main_string as NSString).rangeOfString(string_to_color)
      var attributedString = NSMutableAttributedString(string:main_string)
      attributedString.addAttribute(NSForegroundColorAttributeName, value: Color.themeColor , range: range)
      return attributedString
 
  }
  
}