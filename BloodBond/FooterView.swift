//
//  FooterView.swift
//  BloodBond
//
//  Created by Muzahidul Islam on 10/28/15.
//  Copyright (c) 2015 iMuzahid. All rights reserved.
//

import UIKit

class FooterView: UIView {
  var spinner: UIActivityIndicatorView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = UIColor.clearColor()
    self.spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    spinner.frame = CGRectMake((CGRectGetWidth(frame)-40)/2, 5.0, 40.0, 40.0)
    spinner.hidesWhenStopped = true
    self.addSubview(spinner)
   
  }
  required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
  }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
