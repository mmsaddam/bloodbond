##BloodBond
========================

[![Platform](http://img.shields.io/badge/platform-ios-blue.svg?style=flat
             )](https://developer.apple.com/iphone/index.action)
[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg?style=flat
            )](http://mit-license.org)

![sample](screenshots/img1.png)
![sample](screenshots/img2.png)


##Language

Swift

##Tools
Xcode 6.3, Cocoa Touch

##Requirement
iOS 7 (minimum)
Only for iPhone 5,iPhone 6, iPhone 6 Plus


##About The App
Blood donation is a noble, selfless service! It gives the donor a feeling of joy and contentment. Also this is an expression of love for Mankind, as blood knows no caste, colour, creed, religion or race, country, continent or sex. Above all your blood can save a life

## Contributing

Forks, patches and other feedback are welcome.

## Creator

[Muzahidul Islam](http://mmsaddam.github.io/) 
[Blog](http://mmsaddam.github.io/)

## License

BloodBond is available under the MIT license. See the LICENSE file for more info.